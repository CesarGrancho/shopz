<!DOCTYPE html>
<html lang="en">

<head>
    <?php include $fullPath."/_sections/head.inc"; ?>
    
    <!-- page css : begin -->
        
    <!-- page css : end -->	  
  
  <title>shopZ - My Shop Name</title>
</head>

<body class="theme-red">
  
	<?php include $fullPath."/_sections/topElements.inc"; ?>

    <section>
        <!-- Left Sidebar -->
		<?php //include $fullPath."/_sections/menu.inc"; ?>
        <aside id="leftsidebar" class="sidebar">
		
			<?php include $fullPath."/_sections/userInfo.inc"; ?>

			<?php include $fullPath."/_sections/menu.inc"; ?>

			<?php include $fullPath."/_sections/footer.inc"; ?>			
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
			<?php include $fullPath."/_sections/rightSideBar.inc"; ?>			
		</aside>
        <!-- #END# Right Sidebar -->
    </section>

    <section class="content">
		<div class="body">
			<ol class="breadcrumb">
				<li class="active">Home</li>
			</ol>
		</div>
		
        <div class="container-fluid">
            <div class="block-header">
                <h2>BLANK PAGE</h2>
            </div>
			
			<ul>
				<li>Sells</li>
				<li>- Orders</li>
				<li>- Flash Sell</li>
			</ul>

			<ul>
				<li>Products</li>
				<li>- List</li>
				<li>- Categories</li>
			</ul>
			
			<ul>
				<li>Reports</li>
				<li>- Sales</li>
			</ul>			
			
			<ul>
				<li>Media</li>
				<li>- Gallery</li>
				<li>- Sliders</li>
			</ul>			
			
			<ul>
				<li>Settings</li>
				<li>- Shop</li>
				<li>- Payments</li>
				<li>- Notifications</li>
				<li>-- Email</li>
				<li>-- SMS</li>
				<li>- Users</li>				
				<li>- Languages</li>
			</ul>			
			
        </div>
    </section>

	
	
	<?php include $fullPath."/_sections/footerScripts.inc"; ?>    
	
	<!-- page scripts : begin -->
		
	<!-- page scripts : end -->	
  </div>
</body>

</html>
