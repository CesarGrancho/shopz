var Login = function () {
    
	var handleLogin = function() {

		$('#login-form input').keypress(function (e) {
			if (e.which == 13) {
				$('#login-form-submit').trigger('click');
				return false;
			}
		});
        
        $('#login-form-submit').click(function(e){
            //console.log($('#login-form').serialize());return false;
			e.preventDefault();
			
            $.ajax({
                type:'POST',
                encoding:"UTF-8",        
                dataType: 'json',
                cache: false,        
                url: API,
                data: $('#login-form').serialize(),
                            beforeSend:function(){
                                // this is where we append a loading image
                                //Btn.button('loading');                                
                               
                              },                            
                success: function(r) {                   

					if (r.RESPONSE=="25") {
						$.notify({
							title: '<strong>Invalid Login!</strong>',
							message: 'Please try again!'
						},{
							type: 'warning'
						});						
						
					}
					else if (r.RESPONSE=="1") {
						$.notify({
							title: '<strong>Success!</strong>',
							message: 'Logging...'
						},{
							type: 'success'
						});						
						
						$(this).delay(1500).queue(function() {location.reload();});                         
					}	
					else {
						$.notify({
							title: '<strong>API Error!</strong>',
							message: 'Something went wrong!'
						},{
							type: 'danger'
						});							
					}                            
                    return false;   								  
                                
                },
                error:function(){
                    // failed request; give feedback to user
						$.notify({
							title: '<strong>Application Error!</strong>',
							message: 'Please try again!'
						},{
							type: 'danger'
						});						
                  }                            
            });
            
            return false;
        });	
	        
        
    };
        
        
    
    return {
        //main function to initiate the module
        init: function () {
            
            handleLogin();
            //handleForgetPassword();
            //handleRegister();    
        }

    };

}();

jQuery(document).ready(function() {
    
    Login.init();
});  