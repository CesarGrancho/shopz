<!DOCTYPE html>
<html>

<head>
    <?php include $fullPath."/_sections/head.inc"; ?>
    
    <!-- page css : begin -->
        
    <!-- page css : end -->		
	
	<title>shopZ - Login</title>
</head>

<body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);">shop<b>Z</b></a>
            <small>Open Source E-Commerce Platform for CryptoCoins</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="login-form" method="POST">
                    <div class="msg">Sign in to start your session</div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="loginForm_email" placeholder="Username" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="loginForm_password" placeholder="Password" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
							<input name="c" type="hidden" value="users.do.usersLogin">
							<input name="formAlias" type="hidden" value="loginForm_">						
                            <button class="btn btn-block bg-pink waves-effect" type="submit" id="login-form-submit">SIGN IN</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="#">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
                            <a href="#">Forgot Password?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

  
	<!-- page scripts : begin -->
	<?php include $fullPath."/_sections/footerScripts.inc"; ?>    
	
	<script src="./assets/frontend/views/admin/login/index.js" type="text/javascript"></script>

	<!-- page scripts : end -->					  
  

</body>

</html>		

