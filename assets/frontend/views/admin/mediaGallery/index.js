var imageGallery = function () {

    var imagesGet = function() {
		//console.log("trying to get images...");
		if ($('#media-image-gallery').data('lightGallery')) {			
			$('#media-image-gallery').data('lightGallery').destroy(true);
		}		
		
        $("#media-image-gallery").html("Loading...");
		
		
        $.getJSON(baseURL, { c: "fileManager.do.imagesGet" }, function(j)
        {            
            if (!j.results) {                
				$.notify({
					title: '<strong>'+j.ERROR+'</strong>',
					message:  + '<br>' + j.__errorLog
				},{
					type: 'warning'
				});			

				$("#media-image-gallery").html(j.ERROR);			
            }
			
			$("#media-image-gallery").html("");
			$("#mediaGallery_results").html(j.results); //show number of results			
			
			
			//add elements 
			var template = $("#media-image-gallery-template").html();
			$.each(j.data, function(i, data) {
				//console.log(data.idMedia);
				
				$("#media-image-gallery").append(template);
				
								
				$("#media-image-gallery .media-image-gallery-item:last-child").find("a").attr({
																						"href" : data.fileUrl,
																						"data-sub-html": data.fileDescription,
																						"data-img-tags": data.fileTags
																						});
				
				$("#media-image-gallery .media-image-gallery-item:last-child").find("a img").attr({"src" : data.fileUrl});
				
				$("#media-image-gallery .media-image-gallery-item:last-child").find("a button").attr("idFile", data.idFile);

			});						

			$('#media-image-gallery').lightGallery({
				thumbnail: true,
				selector: 'a'
			});
			
			//DELETE			            
			$('#media-image-gallery a button').click(function(e){
				e.preventDefault();				
				//console.log('idMedia: ' + $(this).attr("idMedia"));
				var fileItems = [$(this).attr("idFile")];				
				
				if (imagesDelete(fileItems)) {
					$(this).parents(".media-image-gallery-item").fadeOut(1000);					
				}
				
				return false;
			});						
			
        });

    };
	
	var imagesDelete = function(fileItems) {
		//console.log(baseURL+"?c=fileManager.do.imagesDelete&fileItems="+JSON.stringify(fileItems));return false;
		
		return $.getJSON(baseURL, { c: "fileManager.do.imagesDelete", fileItems: JSON.stringify(fileItems) }, function(j) {                
			
			if(j.RESPONSE=="25") {
				$.notify({
					title: '<strong>'+j.ERROR+'</strong>',
					message:  j.__errorLog
				},{
					type: 'warning'
				});	
			}
			
			if (j.RESPONSE=="1") {
				$.notify({
					title: '<strong>'+j.ERROR+'</strong>',
					message: j.__errorLog
				},{
					type: 'success'
				});	
				
				return true;
				
			}			
			
			return false;
		});  		
		
		
	};
    	
    
    return {
        //main function to initiate the module
        init: function () {
			
			this.updateWorkspace();			
			
        },
		updateWorkspace: function () {
			
			//get images			
			imagesGet();
        }

    };

}();

jQuery(document).ready(function() {
    
    imageGallery.init();
});  