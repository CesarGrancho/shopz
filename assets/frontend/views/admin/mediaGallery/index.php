<!DOCTYPE html>
<html lang="en">

<head>
    <?php include $fullPath."/_sections/head.inc"; ?>
    
    <!-- page css : begin -->
		<!-- Light Gallery Plugin Css -->
		<link href="./assets/frontend/views/admin/_libs/plugins/light-gallery/css/lightgallery.css" rel="stylesheet">        
		
		<!-- Dropzone Css -->
		<link href="./assets/frontend/views/admin/_libs/plugins/dropzone/dropzone.css" rel="stylesheet">		
    <!-- page css : end -->	  
  
  <title>shopZ - My Shop Name</title>
</head>

<body class="theme-red">
  
	<?php include $fullPath."/_sections/topElements.inc"; ?>

    <section>
        <!-- Left Sidebar -->
		<?php //include $fullPath."/_sections/menu.inc"; ?>
        <aside id="leftsidebar" class="sidebar">
		
			<?php include $fullPath."/_sections/userInfo.inc"; ?>

			<?php include $fullPath."/_sections/menu.inc"; ?>

			<?php include $fullPath."/_sections/footer.inc"; ?>			
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
			<?php include $fullPath."/_sections/rightSideBar.inc"; ?>			
		
		</aside>
        <!-- #END# Right Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">	
			<div class="body">
				<ol class="breadcrumb">
					<li><a href="#">Media</a></li>
					<li class="active">Image Gallery</li>
				</ol>		
			</div>			

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                IMAGE GALLERY                                
								<small>Total: <span id="mediaGallery_results"></span></small>
                            </h2>
					
							<div class="header-dropdown">
								<button type="button" class="btn bg-blue-grey waves-effect" data-toggle="modal" data-target="#mediaGallery_modal_addNew">
									<i class="material-icons">add</i>
									<span>Add New Images</span>									
								</button>								
							</div>
                        </div>
                        <div class="body">
                            <div id="media-image-gallery" class="list-unstyled row clearfix">						
						


                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
        </div>
    </section>
	
	<!-- page resources : begin -->
	<div style="display: none;">
		<div id="media-image-gallery-template">
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 media-image-gallery-item">
				<a href="" data-sub-html="Demo Description" data-img-tags="">
					<img class="img-responsive thumbnail" src="">
					<div class="media-image-gallery-delete">
						<button href="#" type="button" class="btn btn-danger btn-circle-lg waves-effect waves-circle waves-float" idFile="">
							<i class="material-icons">delete_forever</i>
						</button>								
					</div>										
				</a>
			</div>
		</div>	
	</div>
	
	<div class="modal fade" id="mediaGallery_modal_addNew" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h4 class="modal-title">Add New Media</h4>
				</div>
				<div class="modal-body">
					<form action="#" class="dropzone" id="mediaGalleryAddNewDropzone" enctype="multipart/form-data">
						<div class="dz-message">
							<div class="drag-icon-cph">
								<i class="material-icons">touch_app</i>
							</div>
							<h3>Drop files here</h3>
							<em>or click to upload</em>
						</div>
						<div class="fallback">
							<input name="file" type="file" multiple />
						</div>
					</form>
				</div>
				<div class="modal-footer">					
					<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
				</div>
			</div>
		</div>
	</div>	

	
	
	<?php include $fullPath."/_sections/footerScripts.inc"; ?>    
	
	<!-- page scripts : begin -->
	
    <!-- Light Gallery Plugin Js -->
    <script src="./assets/frontend/views/admin/_libs/plugins/light-gallery/js/lightgallery-all.js"></script>	
	
    <!-- Dropzone Plugin Js -->
    <script src="./assets/frontend/views/admin/_libs/plugins/dropzone/dropzone.js"></script>	
	
	<script src="./assets/frontend/views/admin/mediaGallery/index.js" type="text/javascript"></script>
	<script src="./assets/frontend/views/admin/mediaGallery/mediaGalleryAddNew.js" type="text/javascript"></script>
		
	<!-- page scripts : end -->	
  </div>
</body>

</html>
