var mediaGalleryAddNew = function () {
   
    return {
        //main function to initiate the module
        init: function () {        	            
            Dropzone.autoDiscover = false;
           
            //DROP ZONE : begin
            
            var dropZone=new Dropzone( "#mediaGalleryAddNewDropzone", {
                url: API + "c=fileManager.do.imageUpload",
                maxFilesize: 100,
                uploadMultiple: true,
                acceptedFiles: "video/*,audio/*,image/*,.psd,.pdf,.doc,docx",
                init: function() {
					
                    this.on("success", function(file, response) {  

				        var j = jQuery.parseJSON(response);
					
                        try {    
						
                            if(j.RESPONSE=="1") {
								$.notify({
									title: '<strong>'+j.ERROR+'</strong>',
									message: j.__errorLog
								},{
									type: 'success'
								});
                                imageGallery.updateWorkspace();								

                            } 
							
                            if (j.RESPONSE=="25") {
								$.notify({
									title: '<strong>'+j.ERROR+'</strong>',
									message:  j.__errorLog
								},{
									type: 'warning'
								});	
                            }
														
                        }
                        catch (e) {
							$.notify({
								title: '<strong>ERROR</strong>',
								message:  e
							},{
								type: 'warning'
							});	                                                  
							
                        }   

                        setTimeout(function() {dropZone.removeAllFiles();}, 5000);                          
                    });
                    
                }                    
            });

                 

        }

    };    
    
}();


jQuery(document).ready(function() {    
    mediaGalleryAddNew.init();
});  