var productCategories_addNew = function () {
	
    var addNew = function(form) { 
		
		if (!form.valid()) return false;
		
		//console.log(" :: " +form.serialize());return false;
		
        $.ajax({
            type:'POST',
            encoding:"UTF-8",        
            dataType: 'json',
            cache: false,        
            url: API,
            data: form.serialize(),
            beforeSend:function(){
                    // this is where we append a loading image
                    //Btn.button('loading'); 
            },                            
            success: function(j) {
                //console.log(j);
                if (j.RESPONSE=="1") {
                    productCategories.updateWorkspace();
					$.notify({
						title: '<strong>'+j.ERROR+'</strong>',
						message: j.__errorLog
					},{
						type: 'success'
					});                    
					
                    $('#productCategories_modal_addNew').modal('toggle'); 
                    form[0].reset(); 
                }
                else {
					$.notify({
						title: '<strong>'+j.ERROR+'</strong>',
						message:  j.__errorLog
					},{
						type: 'warning'
					});	
                }

                
            },
            error:function(j){
				$.notify({
					title: '<strong>'+j.ERROR+'</strong>',
					message:  j.__errorLog
				},{
					type: 'warning'
				});	
                console.log(j);
            }                            
        });
        
    };	
   
    return {
        //main function to initiate the module
        init: function () {  

			
			$('#productCategories_modal_addNew_form').validate({
				highlight: function (input) {
					$(input).parents('.form-line').addClass('error');
				},
				unhighlight: function (input) {
					$(input).parents('.form-line').removeClass('error');
				},
				errorPlacement: function (error, element) {
					$(element).parents('.form-group').append(error);
				}
			});	

            $("#productCategories_modal_addNew_form").submit(function( e ) {                                
                addNew($(this));                
                e.preventDefault();
                return false;
              });			
			
        }

    };    
    
}();


jQuery(document).ready(function() {    
    productCategories_addNew.init();
});  