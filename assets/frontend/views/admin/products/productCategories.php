<!DOCTYPE html>
<html lang="en">

<head>
    <?php include $fullPath."/_sections/head.inc"; ?>
    
    <!-- page css : begin -->
		<!-- JQuery DataTable Css -->
		<link href="./assets/frontend/views/admin/_libs/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">        
		
		<!-- Sweet Alert Css -->
		<link href="./assets/frontend/views/admin/_libs/plugins/sweetalert/sweetalert.css" rel="stylesheet" />		
    <!-- page css : end -->	  
  
  <title>shopZ - My Shop Name</title>
</head>

<body class="theme-red">
  
	<?php include $fullPath."/_sections/topElements.inc"; ?>

    <section>
        <!-- Left Sidebar -->
		<?php //include $fullPath."/_sections/menu.inc"; ?>
        <aside id="leftsidebar" class="sidebar">
		
			<?php include $fullPath."/_sections/userInfo.inc"; ?>

			<?php include $fullPath."/_sections/menu.inc"; ?>

			<?php include $fullPath."/_sections/footer.inc"; ?>			
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
        <aside id="rightsidebar" class="right-sidebar">
			<?php include $fullPath."/_sections/rightSideBar.inc"; ?>			
		</aside>
        <!-- #END# Right Sidebar -->
    </section>

    <section class="content">
		<div class="body">
			<ol class="breadcrumb">
				<li><a href="#">Products & Services</a></li>				
				<li class="active">List Categories</li>
			</ol>
		</div>
		
        <div class="container-fluid">
			
            <!-- Exportable Table -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                List Categories
                            </h2>

							<div class="header-dropdown">
								<button type="button" class="btn bg-blue-grey waves-effect" data-toggle="modal" data-target="#productCategories_modal_addNew">
									<i class="material-icons">add</i>
									<span>Add New Category</span>									
								</button>								
							</div>							
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable" id="productCategories_tableList">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Description</th>
                                            <th>Active</th>
                                            <th>Tools</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                            <th>ID</th>
                                            <th>Description</th>
                                            <th>Status</th>
                                            <th>Tools</th>
                                        </tr>
                                    </tfoot>
									<tbody>
									</tbody>
								</table>
                            </div>
							
							<!-- datatable element Templates : begin -->
							
							<div id="productCategories_tableList_template_checkbox" style="display: none">								
								<input type="checkbox" class="filled-in chk-col-blue-grey productCategories_tableListDetails_changeStatus" data-idproductcategory=""/>
								<label></label>
							</div>	
							
							<div id="productCategories_tableList_template_tools" style="display: none">
								<a href="#spCategories_Edit_dialog" data-toggle="modal" data-idspcategory="" class="spCategories_tableListDetails_edit" ><i class="fa fa-edit"></i></a>
								<a href="#" data-idspcategory="" class="spCategories_tableListDetails_remove"><i class="fa fa-remove"></i></a>
							</div>		
							
							<!-- datatable element Templates : end -->							
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Exportable Table -->			
			
        </div>
    </section>

	
	<div class="modal fade" id="productCategories_modal_addNew" tabindex="-1" role="dialog">
		<form id="productCategories_modal_addNew_form" class="form" action="#">
	
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Add New Category</h4>
					</div>
					<div class="modal-body">
						<div class="form-group form-float">
							<div class="form-line">
								<input type="text" class="form-control" name="productCategories_AddNew_productCatDescription" required>
								<label class="form-label">Description</label>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<input type="hidden" value="products.do.categoriesAddNew" name="c">
						<input type="hidden" value="productCategories_AddNew_" name="sectionAlias">
					
						<button type="submit" class="btn btn-primary waves-effect">SAVE CHANGES</button>
						<button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
					</div>
				</div>
			</div>
		
		</form>
	</div>	
	
	
	<?php include $fullPath."/_sections/footerScripts.inc"; ?>    
	
	<!-- page scripts : begin -->
	
		<!-- Jquery Validation Plugin Css -->
		<script src="./assets/frontend/views/admin/_libs/plugins/jquery-validation/jquery.validate.js"></script>	
	
		<!-- Sweet Alert Plugin Js -->
		<script src="./assets/frontend/views/admin/_libs/plugins/sweetalert/sweetalert.min.js"></script>	
	
		<!-- Jquery DataTable Plugin Js -->
		<script src="./assets/frontend/views/admin/_libs/plugins/jquery-datatable/jquery.dataTables.js"></script>
		<script src="./assets/frontend/views/admin/_libs/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
		<script src="./assets/frontend/views/admin/_libs/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
		<script src="./assets/frontend/views/admin/_libs/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
		<script src="./assets/frontend/views/admin/_libs/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
		<script src="./assets/frontend/views/admin/_libs/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
		<script src="./assets/frontend/views/admin/_libs/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
		<script src="./assets/frontend/views/admin/_libs/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
		<script src="./assets/frontend/views/admin/_libs/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>	
		
		
		<script src="./assets/frontend/views/admin/products/productCategories.js" type="text/javascript"></script>
		<script src="./assets/frontend/views/admin/products/productCategories_addNew.js" type="text/javascript"></script>
	<!-- page scripts : end -->	
  </div>
</body>

</html>
