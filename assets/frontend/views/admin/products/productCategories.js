var productCategories = function () {
   
	var productCategoriesList = function() {
		$('#productCategories_tableList').DataTable({
			dom: 'Bfrtip',
			responsive: true,
			buttons: [
				'copy', 'csv', 'excel', 'pdf', 'print'
			],
			ajax: {
					"url": baseURL,
					"data": {
						"c": "products.do.categoriesGet"
					}
			},
			columns: [
                {"data": "idProductCategory", "name": "ID", "title": "ID"},
				{"data": "productCatDescription", "name": "Description", "title": "Description"},
				{"data": "productCatActive", "name": "Status", "title": "Status"},
                {"data": "tools", "name": "Tools", "title": "Tools"}
			],
			order: [[1, 'asc']],
            initComplete: function() {
                dataReplacer();
				//console.log("all done");
            } 			
		});
    };      
   
    var dataReplacer = function() {
        var checkBox=$("#productCategories_tableList_template_checkbox").html();
        var active=0;
        var id;
        //var tools=$("#spCategories_tableListDetailsModel_tools").html();
        $.each($('#productCategories_tableList').dataTable().fnGetNodes(), function() {
            id=$(this).find("td:eq(0)").text();
            
            //ADD CHECKBOX
            active=$(this).find("td:eq(2)").html();
            $(this).find("td:eq(2)").html(checkBox);
            
            $(this).find("td:eq(2)").find('input').attr('data-idproductcategory', id);			
			$(this).find("td:eq(2)").find('input').attr('id', "idproductcategory-"+id);
			$(this).find("td:eq(2)").find('label').attr('for', "idproductcategory-"+id);
            
            if (active==='1') {//checked
                $(this).find("td:eq(2)").find('input').attr('checked', 'checked');                        
				$(this).find("td:eq(2)").find('label').html("Active");
            }
            /*
            //ADD TOOLS                    
            $(this).find("td:eq(4)").css({"font-size":'18px'});
            $(this).find("td:eq(4)").html(tools);
            
            $(this).find("td:eq(4)").find('.spCategories_tableListDetails_edit').attr("data-idspcategory",id);
            $(this).find("td:eq(4)").find('.spCategories_tableListDetails_remove').attr("data-idspcategory",id);
            */
            
        });  
		
		
    };
    
    var remove = function(id) {
        //console.log('lets remove: '+ idStore);
        
		$.getJSON(API, { cmd: "storesProducts.do.categoriesRemove", idSPCategory: id }, function(data) {
            if (data.RESPONSE=="1") {
                categories.updateWorkspace();
                toastrNotification('warning', '<h4>Demo Version!</h4> <p>Feature is disabled</p>');
            }
            else {
                toastrNotification('error', "<h4>Something went wrong!</h4> <p>Please try again or contact support!</p>");
            }            
        });
        
    };
    
    var updateStatus = function(checkbox) {
		
        console.log('update status: '+ checkbox.data('idproductcategory') + '-' + checkbox.is(":checked"));return false;
        
		$.getJSON(API, { 
						c: "products.do.categoriesUpdateStatus", 
						idProductCategory: checkbox.data('idproductcategory'), 
						productCatActive: checkbox.is(":checked") 						 
					},
			function(data) {
				if (data.RESPONSE=="1") {      
				
					var checkBoxLabel=checkbox.is(":checked")?"Active":"";			
					checkbox.parents("td").find("label").html(checkBoxLabel);		
					
					$.notify({
						title: '<strong>'+j.ERROR+'</strong>',
						message: j.__errorLog
					},{
						type: 'success'
					});  				
				}
				else {
					$.notify({
						title: '<strong>'+j.ERROR+'</strong>',
						message:  j.__errorLog
					},{
						type: 'warning'
					});	
				}
        });
        
    };   
   
    return {
        //main function to initiate the module
        init: function () {        	            
			productCategoriesList();			
            
			
            $('#productCategories_tableList').on('click','.spCategories_tableListDetails_remove' ,function() {                
                remove($(this).data('idspcategory'));
            });
            
			
			
            $('#productCategories_tableList').on('click','.productCategories_tableListDetails_changeStatus' ,function() {                
				updateStatus($(this));
            });        
			
        },
        updateWorkspace: function() {
            //categories.updateWorkspace()
            $("#spCategories_tableList").DataTable().ajax.reload(function(){dataReplacer();});
        }

    };    
    
}();


jQuery(document).ready(function() {    
    productCategories.init();
});  