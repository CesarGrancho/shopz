            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
                    <li class="active">
                        <a href="?c=users.admin">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
					
				   <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">style</i>
                            <span>Products & Services</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="?c=products.admin">List</a>
                            </li>
                            <li>
                                <a href="?c=products.admin.productCategories">Categories</a>
                            </li>							
                        </ul>
                    </li>						
                   
				   <li>
                        <a href="javascript:void(0);" class="menu-toggle">
                            <i class="material-icons">perm_media</i>
                            <span>Media</span>
                        </a>
                        <ul class="ml-menu">
                            <li>
                                <a href="?c=mediaGallery.admin">Images</a>
                            </li>
                        </ul>
                    </li>
				
					<li class="header">LABELS</li>
                    <li>
                        <a href="javascript:void(0);">
                            <i class="material-icons col-light-blue">donut_large</i>
                            <span>Information</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->