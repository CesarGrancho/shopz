<!-- BEGIN JAVASCRIPT -->
  
    <!-- Jquery Core Js -->
    <script src="./assets/frontend/views/admin/_libs/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="./assets/frontend/views/admin/_libs/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Select Plugin Js -->
    <script src="./assets/frontend/views/admin/_libs/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="./assets/frontend/views/admin/_libs/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
	
    <!-- Bootstrap Notify Plugin Js -->
    <script src="./assets/frontend/views/admin/_libs/plugins/bootstrap-notify/bootstrap-notify.js"></script>		

    <!-- Waves Effect Plugin Js -->
    <script src="./assets/frontend/views/admin/_libs/plugins/node-waves/waves.js"></script>	

    <!-- Custom Js -->
    <script src="./assets/frontend/views/admin/_libs/js/core.js"></script>  	
	

	
<!-- END JAVASCRIPT -->