<?php

/*******************************************************************************
 *                      Application Function Class
 *******************************************************************************
 *      Author:     César Pinto
 *      Email:      cesar.grancho@live.com.pt
 *      Website:    https://www.linkedin.com/in/cesargrancho/
 *
 *      File:       app.class.php
 *      Version:    1.1.1
 *      Copyright:  (c) 2017 César Pinto
 *                  
 *      License:    MIT
 *
 *******************************************************************************
 *  VERSION HISTORY:
 ******************************************************************************* 
 *      v1.1.3 [18.07.2018] 
 *		- Add DevMode Debug Output
 *      	USAGE:
 *      	In API Response ( if devMode TRUE will pass the debug into JSON otherwise will be clear )
 *      	$API->__errorLog=($this->_devMode?$this->_CLASS->__errorLog:"")
 *
 *      v1.1.2 [15.12.2017] 
 *		- Fix default routing  files
 *
 *      v1.1.1 [18.11.2017] 
 *		- Permissions struture changes - moved into users class
 *
 *      v1.1.0 [16.11.2017]
 *      - Front-End Structure changes
 *
 *      v1.0.5 [8.11.2017]
 *      - Construct INIT changes, all vars now properly in array
 *      
 *      v1.0.4 [5.11.2017]
 *      - Support for SQLite
 *      
 *      v1.0.3 [28.10.2017]
 *      - Fixed session id conflict for multiple applications in same server
 *
 *      v1.0.2 [16.10.2017]
 *      - Add Memory and Execution limits
 *
 *      v1.0.0 [6.9.2017] 
 *		- Public Version Release
 *
 *******************************************************************************
 *  DESCRIPTION:
 *******************************************************************************
 *  The class provides basic configuration and initialization for the application
 *  
 *
 *******************************************************************************
 *  Functions Tree:
 *******************************************************************************
 *  
 *  .PUBLIC
 *  - init
 *  - routing
 *  
 *  .PRIVATE
 *  - initSystem
 *  -- sessionInit
 *  -- headersInit
 *  -- timezoneInit
 *  
 *  - initApp
 *  -- databaseInit
 *
 *  
 *******************************************************************************
 *  USAGE:
 *******************************************************************************
 *
 *  $this->dbOperations()
 *  
 *******************************************************************************
*/

class app  {
    
    public $_appData;  # Array of app vars
    
    public $_APIActions; # used for view actions
    
    public $__errorLog; # USED to PARSE & DISPLAY ERRORS
    
    public $__ROOT; # dirname(__FILE__)
    
    #EXTERNAL OBJECTS
    public $_AUX;
    
    ###########################
    # PUBLIC FUNCTIONS : BEGIN
    ###########################    
    
    public function __construct($data = array())
    {
        $this->_appData=array();        
        
        $this->__errorLog="";        

        $this->_AUX=$data['aux'];
        $this->_SESSION=$data['session'];
        $this->__ROOT=$data['root'];        
    }
    
    public function init($data = array()) {
        
        if (!$this->initSystem($data['system'])) {
            return false;
        }
        
        if (!$this->initApp($data['app'])) {
            return false;
        }        
        
        return true;
        
    }
    
    
    public function routing($routeData = "") {
        
        #echo $routeData;die();
        $c=explode('.',$routeData);

            /*
            //ROUTE STRUTURE
			ie: model.tools.operation - API PANDIX TOOLS REQUESTS
            ie: model.do.operation - API CLASS EXTENSION REQUESTS
            ie: model.view.section.page - LAYOUT OUTPUTS
            |-SECTION
            |--DO
            |---ACTIONS
            |--TOOLS
            |---ACTIONS
            |--VIEWS
            |---ACTIONS
            */        
			
			
			#BACK END DEFAULTS : begin
			$_classExtensions=$this->_appData['PATHING']['BACKEND']['EXTENDS'];
			$_classTools=$this->_appData['PATHING']['BACKEND']['TOOLS'];
			#BACK END DEFAULTS : end
			
			#FRONT END Defaults : begin	
			$dir=$this->_appData['PATHING']['FRONTEND']['DIR'];
			$section=$this->_appData['PATHING']['FRONTEND']['DEFAULTS']['SECTION'];
			$mode=$this->_appData['PATHING']['FRONTEND']['DEFAULTS']['MODE'];		
			$actions=$this->_appData['PATHING']['FRONTEND']['DEFAULTS']['ACTION'];			
			#FRONT END Defaults : end				
            
            if (count($c) > 0 
			&&$this->_AUX->stringValidation($c[0])) {
                #SECTION
                $section=$c[0];
                    
                #MODE - do:view:mobile:...
                if ($this->_AUX->stringValidation($c[1])) {
                    $mode=$c[1];
                }
                
                #ACTIONS 				
                if ($this->_AUX->stringValidation($c[2])) {
                    $actions=$c[2];                    
                    #used to do operations
                    $this->_APIActions['actions']=$c[2];					
                }                
                
                #echo $section."-".$mode."-".$actions;die();
             
                if ($mode=="do") { #GO TO API OPERATIONS
                    
                    $requiredFile=sprintf("%s/%s/%s.api.php",
										$_classExtensions,										
										$section,
										$section
                                       );
                    
                }
                else if ($mode=="tools") { #GO TO API PandIX TOOLS                    
                    
                    
                    $requiredFile=sprintf("%s/%s/%s.api.php",
										$_classTools,
										$section,
										$section
										);                    
                }
                else {	
				
					$fullPath=sprintf("%s/%s/%s",
										ROOT,
										$dir,
										$mode); 	
										
					$requiredFile=sprintf("%s/%s/%s.php",
										$fullPath,
										$section,
										$actions);      

                }
                
            }
            else {			
				$fullPath=sprintf("%s/%s/%s/",
									ROOT,
									$dir,
									$mode #site, dashboard, admin, other...
									); 	
									
				$requiredFile=sprintf("%s/%s/%s.php",
									$fullPath,
									$section,
									$actions);    
            }
            
			#echo json_encode(array('Result'=>'25','API_OPERATION_ERROR' => $GLOBAL_LNG['API_OPERATION_ERROR'])); #."-".$_REQUEST['cmd']        			
		
		if (!file_exists($requiredFile)) {
			$fullPath=sprintf("%s/%s/%s/",
								ROOT,
								$this->_appData['PATHING']['FRONTEND']['DIR'],
								$this->_appData['PATHING']['FRONTEND']['DEFAULTS']['MODE']
								); 			
			
									
			$requiredFile=sprintf("%s/%s/%s.php",
								$fullPath,
								$this->_appData['PATHING']['FRONTEND']['DEFAULTS']['SECTION'],
								$this->_appData['PATHING']['FRONTEND']['DEFAULTS']['ACTION']); 
		}      
		
        require $requiredFile;
               
    }

    ###########################
    # PUBLIC FUNCTIONS : BEGIN
    ###########################    
    
    ###########################
    # PRIVATE FUNCTIONS : BEGIN
    ###########################
    
    #system functions : begin
        private function initSystem($data = array()) {
            
            try {
				
				$this->_systemData=$data;
				
				#dev mode : begin 
				if ($this->_systemData['devMode']) {
					ini_set('display_errors', 1);
					ini_set('display_startup_errors', 1);

					error_reporting(E_ALL & ~E_NOTICE ^ E_DEPRECATED);				
				}
				else 
				{
					ini_set('display_errors', 0);
					ini_set('display_startup_errors', 0);

					error_reporting(0);
				}
				
				#dev mode : end
                
                #memory Limits : begin
                $appMemory=$data['memory'];
                if (is_numeric($appMemory)
                    && $appMemory>0) {
                    ini_set('memory_limit',$appMemory);
                }                
                #memory Limits : end
                
                #timeout Limits : begin
                $appTimeout=$data['timeout'];
                if (is_numeric($appTimeout)
                    && $appTimeout>0) {                    
                    ini_set('max_execution_time', $appTimeout);
                }                
                #timeout Limits : end                
                
                
                
                $appType=$data["type"];				
                #check if app type is valid
                if (!$this->_AUX->stringValidation($appType)) {
                    $this->__errorLog.= sprintf("Error in system type: %s .",
                                                $appType
                                                );
                    return false;
                }
                
                #start methods for each app type : begin						
                if ($appType=="HTTP") {
						
                    #session init
                    if (!$this->sessionInit($data['session'])) {
                        return false;
                    }; 
                    
                    #headers init
                    if (!$this->headersInit($data['headers'])) {
                        return false;
                    };
                    
                    #timezone init
                    if (!$this->timezoneInit($data['timezone'])) {
                        return false;    
                    }
            
                    return true;
                }
                else {
                    $this->__errorLog = sprintf("No support for system type: %s",
                                                $appType
                                                );
                }
                #start methods for each app type : end
                
            } catch (Exception $e) {
                 $this->__errorLog = sprintf("Expection Found: %s",
                                             $e->getMessage()
                                            );
            }
            
        }
        
        #WWW SYSTEM : begin
        
            #session: begin
                private function sessionInit($sessionData = array()) {
                    
                    try {                    
                        $sessionName=$sessionData['name'];
                        #check if session name is valid string
                        if (!$this->_AUX->stringValidation($sessionName)) {
                            $this->__errorLog.= sprintf("Error in session name: %s .",
                                                        $sessionName
                                                        );
                            return false;                   
                        }
                        
                        #session setup
                        if ($_SESSION['siteID'] != $sessionName) {
                            session_destroy();
                            session_start();
                            $_SESSION['siteID']=$sessionName;
                            #echo $_SESSION['siteID'];die();
                        }          
                        
                        #add session time limits
                        $sessionExpiration=$sessionData['expiration'];
                        #check if session expiration is numeric
                        if (!is_numeric($sessionExpiration)) {
                            $this->__errorLog.= sprintf("Error in session expiration: %s .",
                                                        $sessionExpiration
                                                        );
                            return false;                   
                        }           
                           
                        
                        ini_set('session.gc_maxlifetime', $sessionExpiration);
                        session_set_cookie_params($sessionExpiration);
                        
                        return true;
                    
                    } catch (Exception $e) {
                         $this->__errorLog = sprintf("Expection Found: %s",
                                                     $e->getMessage()
                                                    );
                         return false;
                    }                
                }            
            #session: end
            
            #headers: begin
                private function headersInit($headers = array()) {
                    
                    try {
                        
                        #check if header is valid with array count
                        if (count($headers)==0) {
                            $this->__errorLog.= sprintf("Error in headers array: %s .",
                                                        print_r($headers, true)
                                                        );
                            return false;                         
                        }
                        
                        #start output of headers : begin
                        foreach ($headers as $header=>$headerValue) {
                            
                            #check if header title is valid
                            if (!$this->_AUX->stringValidation($header)) {
                                $this->__errorLog.= sprintf("Error in header name: %s .",
                                                            $header
                                                            );
                                continue;                 
                            }
                            
                            #check if header value is valid
                            if (!$this->_AUX->stringValidation($headerValue)) {
                                $this->__errorLog.= sprintf("Error in header value: %s - %s.",
                                                            $header,
                                                            $headerValue
                                                            );
                                continue;                 
                            }                        
                        
                            #format header output
                            $headerOutput=sprintf("%s: %s",
                                                  $header,
                                                  $headerValue
                                                  );
                            
                            #output header
                            #echo $headerOutput."<br>";
                            header($headerOutput);
                            
                            
                        }
                        #start output of headers : end
                        
                        
                        return true;
                    
                    } catch (Exception $e) {
                         $this->__errorLog = sprintf("Expection Found: %s",
                                                     $e->getMessage()
                                                    );
                         return false;
                    }
                    
                }
            #headers : end
            
            #timezone : begin
                private function timezoneInit($timeZone = "") {
                    
                    try {                    
                        date_default_timezone_set($timeZone);
                        
                        return true;
                    
                    } catch (Exception $e) {
                         $this->__errorLog = sprintf("Expection Found: %s",
                                                     $e->getMessage()
                                                    );
                         return false;
                    }                
                }  
            #timezone : end
    
        #WWW SYSTEM : end
    
    #system functions : end
    
    
    #app functions : begin
        private function initApp($appData = array()) {
            
            try {
                
                #assign app values in array
                $this->_appData=$appData;                

                
                #register and load required class              
                if (!$this->classesInit($appData["PATHING"])) {
                    return false;
                };                 
                

                    
				#database init
				if (!$this->databaseInit($appData["DB"])) {
					return false;
				}; 
		
				return true;
                
                
            } catch (Exception $e) {
                 $this->__errorLog = sprintf("Expection Found: %s",
                                             $e->getMessage()
                                            );
            }
            
        }
        
        #www app : begin
        
            #database : begin
                private function databaseInit($dbData = array()) {
                    
                    try {
                        
                        if ($dbData['obdc']==null) return true;
                        
                        if ($dbData['obdc']=="PDO") { #PDO SETTINGS
                            
                            if ($dbData['type']=="mysql") {
                                $DBCON = sprintf('%s:host=%s;dbname=%s',$dbData['type'], $dbData['url'], $dbData['name']);
                                $this->_DBCON = new PDO($DBCON, $dbData['user'], $dbData['pass']);                                
                            } 
							else if ($dbData['type']=="sqlite") {
                                $DBCON = sprintf('%s:%s',$dbData['type'], $dbData['name']);                                
                                
                                $this->_DBCON = new PDO($DBCON);                                       
                            }
							
							$this->_DBCON->query("SET CHARACTER SET utf8");								
                            
                            #$this->_DBCON->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                            $this->_DBCON->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);                            

                        
                        }
                        else {
                            $this->__errorLog = sprintf("No support for obdc type: %s",
                                                        $dbData['obdc']
                                                        );
                            
                            return false;
                        }
                        
                        return true;
                    
                    } catch (Exception $e) {
                         $this->__errorLog = sprintf("Expection Found: %s",
                                                     $e->getMessage()
                                                    );
                         return false;
                    }                
                }  
            #database : end
            
            #classes : begin
                private function classesInit($classesData = array()) {
                    
                    try {
                      
                        function autoload( $class, $dir = null ) {
                        
                            if ( is_null( $dir ) )
                              $dir = "./assets/backend/class/extends/"; #$dir = $this->_appData['PATHING']['CLASSES']; 
                         
                            foreach ( scandir( $dir ) as $file ) {
                                
                              # directory?
                              if ( is_dir( $dir.$file ) && substr( $file, 0, 1 ) !== '.' )
                                autoload( $class, $dir.$file.'/' );
                         
                              # php file?
                              if ( substr( $file, 0, 2 ) !== '._' && preg_match( "/.php$/i" , $file ) ) {
                         
                                # filename matches class?
                                
                                if ( str_replace( '.php', '', $file ) == $class || str_replace( '.class.php', '', $file ) == $class ) {
                                    
                                    include $dir . $file;
                                }
                              }
                            }
                          }                        
                        
                        #try init extended classes                        
                        if (!spl_autoload_register( 'autoload' )) {
                            $this->__errorLog = sprintf("Error loading class: %s",
                                                        $class
                                                        );                            
                            return false;                              
                        }
                        
                        return true;
                    
                    } catch (Exception $e) {
                         $this->__errorLog = sprintf("Expection Found: %s",
                                                     $e->getMessage()
                                                    );
                         return false;
                    }                
                } 
            #classes : end
            
        #www app : end
        
    #app functions : end
    
    ###########################
    # PRIVATE FUNCTIONS : end
    ###########################    
}
?>