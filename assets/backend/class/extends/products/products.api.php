<?php

#Init User Variable - can be override by admin
$_idUser=$this->_USERS->_idUser;

#products CLASS INITIALIZATION
$initProductsData=[
	"db" => $this->_DBCON,
	"aux" => new auxFunctions(),	
	"idUser" => $_idUser
];


#API CLASS INITIALIZATION
$initData=[
	"devMode" => $this->_systemData['devMode'],
	"idUser" => $_idUser,
	"aux" => new auxFunctions(), // CORE Auxiliar Functions
	"output" => "json", // TYPE OF OUTPUT
	"request" => $_REQUEST, // Handle the Current User Request
	"products" => new products($initProductsData) // Init Class Into API
];



#API CALL : begin
$api= new class($initData) {
	//core API
	private $_devMode;
	private $_AUX;
	private $_REQUEST;
	private $_OUTPUT;
	
	//idUser
	private $_idUser;	
	
	//class env
	private $_products;

	
	#CORE API Functions : begin	
	public function __construct($data=array()) {	
		$this->_devMode = $data["devMode"];	
		$this->_AUX = $data["aux"];	
		$this->_REQUEST = $data["request"];
		$this->_OUTPUT = $data["output"];
		
		
		$this->_idUser= $data["idUser"];
						
		
		$this->_products= $data["products"];
	}			
	
	public function apiError($data=array()) {
		$APIResponse["ERROR"]="API Error - Nothing to display";
		$APIResponse["RESPONSE"]="25"; 
		
		return $APIResponse;	
	}
	
	public function output($data=array()) {
	
		switch ($this->_OUTPUT) {
			
			default:
			#json
			return json_encode($data);
		}
		
	}
	#CORE API Functions : end 
	
	
	public function categoriesAddNew($data=array()) {
        /* INIT REST API RETURN OBJECT */
		$API = (object) array(
					"RESPONSE" => 1,
					"ERROR" => 'Success',
					"__errorLog" => ""

		);		
		

		$sectionAlias=$data['sectionAlias'];

		$productsCatData=[
			"idUser" => $this->_idUser,
			"productCatDescription" => $data[$sectionAlias."productCatDescription"],
			"productCatActive" => 1
			
			];		
		
		if (!is_numeric($API->results=$this->_products->productsCatAdd($productsCatData)))
			{ 
			$API->RESPONSE=25; //Error
			$API->ERROR='Error adding new product category.';
			$API->__errorLog=($this->_devMode?$this->_products->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
			return $API;  
		}			
		
		#Return API Answer
		$API->__errorLog=($this->_devMode?$this->_products->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
		return $API; 			
	}

	public function categoriesGet($data=array()) 
	{        
        /* INIT REST API RETURN OBJECT */
		$API = (object) array(
					"RESPONSE" => 1,
					"ERROR" => 'No errors',
					"__errorLog" => "",
					"data" => array()
		);			   				
		
	
		//get user product categories : begin		
			if (!is_array($API->data=$this->_products->productsCatGet([
							'idUser'=> $this->_idUser
						])))
				{ //NOT Numeric
				$API->RESPONSE=25; //Error
				$API->ERROR='Error fetching product categories.';
				$API->__errorLog=($this->_devMode?$this->_products->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
				return $API;  
			}	
			
			//add specific field for data tables
			$i=0;
			while ($i < count($API->data)) {
				$API->data[$i]["tools"]="x";
				$i++;
			}			
			
		//get user product categories : end
		
		#Return API Answer
		$API->__errorLog=($this->_devMode?$this->_products->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
		return $API; 	
	}	

	public function categoriesUpdateStatus($data=array()) {
        /* INIT REST API RETURN OBJECT */
		$API = (object) array(
					"RESPONSE" => 1,
					"ERROR" => 'No errors',
					"__errorLog" => ""
		);		


		#Return API Answer
		$API->__errorLog=($this->_devMode?$this->_products->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
		return $API; 		
	}
};
	
#API CALL : end


#api method exist?
if (!method_exists($api,$this->_APIActions['actions'])) {
	
	echo $api->output($api->apiError());	
	exit();
}

#call dynamic method
echo $api->output($api->{$this->_APIActions['actions']}($_REQUEST));

?>
