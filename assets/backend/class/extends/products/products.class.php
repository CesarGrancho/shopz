<?php

/*******************************************************************************
 *                      PRODUCTS Class
 *******************************************************************************
 *      Author:     César Pinto
 *      Email:      cesar.grancho@live.com.pt
 *      Website:    https://www.linkedin.com/in/cesargrancho/
 *
 *      File:       products.class.php
 *      Version:    1.0.1
 *      Copyright:  (c) 2018 César Pinto
 *                  
 *      License:    MIT
 *
 *******************************************************************************
 *  VERION HISTORY:
 *******************************************************************************
 *
 *      v1.0.1 [23.07.2018] - Refactoring code
 *
 *      v1.0.0 [23.06.2017] - Initial Version
 *
 *******************************************************************************
 *  DESCRIPTION:
 *******************************************************************************
 *  The class got functions for products and its categories operations
 *
 *   -] HOW USE [-
 *******************************************************************************
*/



class products {
    
    public $_productsData; //array with database fields    
	public $_productsCatData; //array with database fields    
	
	public $__errorLog;	
	
    private $_idUser;    
    private $_PDO; //DATABASE OBJECT			
	private $_AUX; //AUXILIAR FUNCTIONS OBJECT
    
    //PUBLIC Function : begin
    
    public function __construct($data)
    {
        $this->_PDO=$data["db"];
		$this->_AUX=$data["aux"];        
        $this->_idUser=isset($data["idUser"])?$data["idUser"]:0;	
		
		$this->_productsData = (object) array(
			"_idStoreProduct" => 0,
			"_idProductCategory" => 0,
			"_name" => "",
			"_images" => "", 
			"_description" => "",
			"_price" => 0,
			"_price_sale" => 0,
			"_sale_start_date" => 0,
			"_sale_end_date" => 0,
			"_active" => 0
		);
		
		$this->_productsCatData = (object) array(
			"_idProductCategory" => 0,
			"_productCatDescription" => "",
			"_productCatActive" => 0
		);
    }
    
	#PRODUCTS CATEGORIES CLASS FUNCTIONS : begin 
    public function productsCatAdd($data=array()) {   
		/*
		 add file items for user
		 requires: 
				idUser
				productCatDescription				
		
		 conditional:
				productCatActive 	
		
		 returns:
		 	false (on error) 
			idProductCategory (new index)		
		*/		
		$this->__errorLog.=("<br>\n productsCatAdd(init)");		
		
		#INIT Class Variables : begin
			
			#idUser validation : begin
				$idUser=isset($data["idUser"])?$data["idUser"]:$this->_idUser;        
			
				#allow only numeric values
				if (!is_numeric($idUser)|| $idUser==0) {
					$this->__errorLog.=sprintf("->(error in idUser: %s)", $idUser);
					return false; 
				}				
			#idUser validation : end
			
			#productCatDescription validation : begin			
				$productCatDescription=isset($data["productCatDescription"])?$data["productCatDescription"]:0;        
				
				#allow only numeric values
				if ($productCatDescription=="") {
					$this->__errorLog.=sprintf("->(error in productCatDescription: %s)",$productCatDescription);
					return false; 
				}					
			#productCatDescription validation : end
			
			#productCatActive conditional validation
			$productCatActive=is_numeric($data["productCatActive"])?$data["productCatActive"]:0;        

			
			#create data array
			$SQLQueryData=[
				":idUser" => $idUser,
				":productCatDescription" => $productCatDescription,
				":productCatActive"=> $productCatActive
			];
			
		#INIT Class Variables : end		
        
		#insert into database : begin
			$SQLQuery="INSERT INTO `productsCategories`
                     (`idProductCategory`, `idUser`, `productCatDescription`, `productCatActive`)
                     VALUES (NULL, :idUser, :productCatDescription, :productCatActive)";

            #echo $this->_AUX->PDODebugger($SQLQuery,$SQLQueryData);die();																
            $SQLResult = $this->_PDO->prepare($SQLQuery);
			
			if (!$SQLResult->execute($SQLQueryData)) {
				$this->__errorLog.=sprintf("->(Error insert product category to Database)[ %s ]",$this->_AUX->PDODebugger($SQLQuery,$SQLQueryData));
				return false;          
			}				            	
		#insert into database : end	
		
		return $this->_PDO->lastInsertId();
        
    }	
	
	public function productsCatGet($data=array()) 
	{
		# get file items for user
		# requires: 
		#		idUser
		#
		# conditional:
		#		productCatActive		
		#
		# returns:
		# 	false (on error) 
		#	array
		
		$this->__errorLog.=("<br>\n productsCatGet(init)");	

		//INIT Class Variables : begin			
			$SQLCondition="";
			
			#idUser validation : begin
				$idUser=isset($data["idUser"])?$data["idUser"]:$this->_idUser;        
				
				#allow only numeric values
					if (!is_numeric($idUser)|| $idUser==0) {
						$this->__errorLog.=sprintf("->(error in idUser: %s)", $idUser);
						return false; 
					}				
					
					$SQLQueryData[":idUser"]=$idUser;	
			#idUser validation : end	

			
			#productCatActive conditional validation : begin
				$productCatActive=!is_numeric($data["productCatActive"])?$data["productCatActive"]:0;        					
				
				#ADD SQL & query array
				if ($productCatActive>0) {
					$SQLQueryData[":productCatActive"]=$productCatActive;				
					$SQLCondition.="AND `a`.`productCatActive`=:productCatActive";								
				}				
			#productCatActive validation : end
			
		//INIT Class Variables : end	
		
		#GET USER PRODUCT CATEGORIES : begin
			#create sql 
			$SQLQuery=sprintf("SELECT * FROM `productsCategories` `a`
						 WHERE `a`.`idUser`=:idUser
						 %s
						 ORDER BY `a`.`idProductCategory` DESC",
						 $SQLCondition
						 );		

						
			//echo $this->_AUX->PDODebugger($SQL,$SQLQueryData);die();																
			$resultQuery = $this->_PDO->prepare($SQLQuery);	
			
			#try get data from database
			if (!$resultQuery->execute($SQLQueryData)) {
				$this->__errorLog.=sprintf("->(error in query to get user product categories)[ %s ]",$this->_AUX->PDODebugger($SQLQuery,$SQLQueryData));
				return false;
			}	
			
			#if has no records return empty array
			if ($resultQuery->rowCount()==0) {
				$this->__errorLog.=("->(no file found for user)");
				return array();
			}
		
		#GET USER PRODUCT CATEGORIES : begin		
		
		return $resultQuery->fetchAll(PDO::FETCH_ASSOC);				
	}		
	
	
	#PRODUCTS CATEGORIES CLASS FUNCTIONS : end 
	
    public function productsList($data = array())
    {
        /* SET GLOBALS VARS LOCAL */
        $_idUser=isset($this->_idUser)?$this->_idUser:0;
        $_idStoreProduct=isset($data['idStoreProduct'])?$data['idStoreProduct']:0;
        
        /* INIT REST API VAR */
        $response=array();
        $response['RESPONSE']=1; //DEFAULT FALSE
        $response['ERROR']='No errors';  //hmm defaults..           
        
        if (isset($_idUser) &&
            is_numeric($_idUser) &&
            $_idUser>0 &&
            !is_null($_idUser)) {
            //CHECK IF USER HAS MALLS

            if (is_numeric($_idStoreProduct)
                && $_idStoreProduct>0) {//ADD QUERY CONDITION FOR SINGLE PRODUCT
                $queryCondition=sprintf("AND `a`.`idStoreProduct` = %s",$_idStoreProduct);
            }
            
            $SQL=sprintf("SELECT `a`.*,                         
                         `b`.`name` as `storeDescription`
                        FROM `storesProducts` `a`
                        JOIN `stores` `b`
                        ON (`a`.`idStore`=`b`.`idStore`)                       
                        WHERE `a`.`idUser`=%s
                        %s
                        ORDER BY `a`.`idStoreProduct` DESC",
                         $_idUser,
                         $queryCondition);            
            
        }
        else { //FALSE NO VALUES PASS                       
            $response['ERROR']=sprintf("NO VALUE PASS - ID USER: %s",$_idUser);
            
            return $response;  
        }        
        
        #echo $SQL;
        $query = $this->_PDO->prepare($SQL);
        $query->execute();        

        $this->__errorLog.=sprintf("QUERY TO LIST Store Products: %s",$SQL);
        
        #echo $this->__errorLog;
        $results=$query->rowCount();
        if ($results>0) { //there is malls
            
            //count query results
            $response['results']=$results;
            
            //ASSIGN DATA
            $i =0;
            while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
                $response['data'][$i]=$row;
                unset($response['data'][$i]['idUser']);
                
                $response['data'][$i]['tools']="E X";
                
                $response['data'][$i]['price']=number_format($response['data'][$i]['price'], 2, '.', '');
                $response['data'][$i]['price_sale']=number_format($response['data'][$i]['price_sale'], 2, '.', '');
                
                //FORMAT DATE
                $dt = new DateTime('@' .$response['data'][$i]['sale_start_date']);                
                $response['data'][$i]['sale_start_date']=$dt->format('Y-m-d');
                $dt = new DateTime('@' .$response['data'][$i]['sale_end_date']);                
                $response['data'][$i]['sale_end_date']=$dt->format('Y-m-d');                
                
                //GET CATEGOREIES DESCRIPTION
                $response['data'][$i]['categories']=$this->productsGetCategories($response['data'][$i]['idSPCategory']);
                
                #MEDIA :begin
                
                //Background Image
                if (is_numeric($response['data'][$i]['imagesbg'])
                    && $response['data'][$i]['imagesbg']>0) {
                    $imagesbg=$this->productsGetImages($response['data'][$i]['imagesbg']);
                    $response['data'][$i]['mediaUrl']=$imagesbg[0]['mediaUrl'];
                    $response['data'][$i]['idMedia']=$response['data'][$i]['imagesbg'];                                     
                }
                
                //Product Images                
                $response['data'][$i]['images']=$this->productsGetImages($response['data'][$i]['images']);
                #First Image
                $response['data'][$i]['image']=$response['data'][$i]['images'][0]['mediaUrl'];
               
                #MEDIA :end
                
                //ADD INPUT FORM NAMES
                if (is_numeric($_idStoreProduct)
                && $_idStoreProduct>0) {
                    $response['data'][$i]['idSPCategory[]']=$response['data'][$i]['idSPCategory'];
                    unset($response['data'][$i]['idSPCategory']);
                    
                    $response['data'][$i]['images[]']=$response['data'][$i]['images'];
                    unset($response['data'][$i]['images']);
                }
                
                
                
                $i++;
            };
        }
        else
        {
            $response['data'][$i]=array();
        }
        
        return $response;        
    }

    public function productsAddNew($data)
    {
        //data sent - do validations
        $sectionAlias=$data['sectionAlias'];
        
        $d=array();
        $d['idUser']=isset($this->_idUser)?$this->_idUser:0;
        $d['name']=isset($data[$sectionAlias.'name'])?$data[$sectionAlias.'name']:"";
        $d['description']=isset($data[$sectionAlias.'description'])?$data[$sectionAlias.'description']:"";
        $d['price']=isset($data[$sectionAlias.'price'])?$data[$sectionAlias.'price']:"";
        $d['price_sale']=isset($data[$sectionAlias.'price'])?$data[$sectionAlias.'price_sale']:"";
        $d['sale_start_date']=strtotime($data[$sectionAlias.'sale_start_date'])?strtotime($data[$sectionAlias.'sale_start_date']):0;
        $d['sale_end_date']=strtotime($data[$sectionAlias.'sale_end_date'])?strtotime($data[$sectionAlias.'sale_end_date']):0;
        $d['idSPCategory']=isset($data[$sectionAlias.'idSPCategory'])?$this->_AUX->formInputArrayToString($data[$sectionAlias.'idSPCategory']):0;        
        $d['idStore']=isset($data[$sectionAlias.'idStore'])?$data[$sectionAlias.'idStore']:0;
        $d['images']=isset($data[$sectionAlias.'images'])?str_replace(",",";",$data[$sectionAlias.'images']):0;
        $d['imagesbg']=isset($data[$sectionAlias.'imagesbg'])?$data[$sectionAlias.'imagesbg']:0;        
        $d['active']=1;
                                                                                                    
        //API Response
        $APIResponse=array();
        $APIResponse['RESPONSE']=1; //Success
        $APIResponse['ERROR']='No Errors'; //No Errors
        
        if (!$this->add($d)) { //SOMETHING WENT WRONG ADDING NEW MALL                        
            $APIResponse['RESPONSE']=25; //Error
            $APIResponse['ERROR']='SOMETHING WENT WRONG ADDING NEW STORE PRODUCT';
        }
        
        return $APIResponse;        
        
        
    }
    
    
    public function productsEdit($data) 
	{
        //data sent - do validations
        $sectionAlias=$data['sectionAlias'];
        
        $d=array();
        $d['idUser']=isset($this->_idUser)?$this->_idUser:0;
        
        
        $d['idStoreProduct']=is_numeric($data[$sectionAlias.'idStoreProduct'])?$data[$sectionAlias.'idStoreProduct']:0;
        $d['name']=isset($data[$sectionAlias.'name'])?$data[$sectionAlias.'name']:"";
        $d['description']=isset($data[$sectionAlias.'description'])?$data[$sectionAlias.'description']:"";
        $d['price']=isset($data[$sectionAlias.'price'])?$data[$sectionAlias.'price']:"";
        $d['price_sale']=isset($data[$sectionAlias.'price'])?$data[$sectionAlias.'price_sale']:"";
        $d['sale_start_date']=strtotime($data[$sectionAlias.'sale_start_date'])?strtotime($data[$sectionAlias.'sale_start_date']):0;
        $d['sale_end_date']=strtotime($data[$sectionAlias.'sale_end_date'])?strtotime($data[$sectionAlias.'sale_end_date']):0;
        $d['idSPCategory']=isset($data[$sectionAlias.'idSPCategory'])?$this->_AUX->formInputArrayToString($data[$sectionAlias.'idSPCategory']):0;        
        $d['idStore']=isset($data[$sectionAlias.'idStore'])?$data[$sectionAlias.'idStore']:0;
        $d['images']=isset($data[$sectionAlias.'images'])?rtrim($data[$sectionAlias.'images'], ';'):0;
        $d['imagesbg']=is_numeric($data[$sectionAlias.'idMedia'])?$data[$sectionAlias.'idMedia']:0;        
        $d['active']=($data[$sectionAlias.'active']=='on')?1:0;
                                                                                                    
        //API Response
        $APIResponse=array();
        $APIResponse['RESPONSE']=1; //Success
        $APIResponse['ERROR']='No Errors'; //No Errors
        
        //API Response
        $response=array();
        $response['RESPONSE']=1; //Success
        $response['ERROR']='No Errors'; //No Errors
        
        if (!$this->edit($d)) { //SOMETHING WENT WRONG ADDING NEW MALL                        
            $APIResponse['RESPONSE']=5002; //Error
            $APIResponse['ERROR']='SOMETHING WENT WRONG EDIT PRODUCT';
        }        
        
        return $response;        
    }
    
       
    public function productsActiveUpdate($data = array()) 
	{
        # INIT REST API : begin
        $APIResponse=array();
        $APIResponse['RESPONSE']=1; //DEFAULT FALSE
        $APIResponse['ERROR']='No errors';  //hmm defaults..
        # INIT REST API : end
        
        # Data Validation 
        $d=array();
        $d['idUser']=is_numeric($this->_idUser)?$this->_idUser:0;
        $d['idStoreProduct']=is_numeric($data['idStoreProduct'])?$data['idStoreProduct']:0;
        $d['active']=($data['active']=='true')?1:0;                        

        
        if ($d['idUser']==0 
            || $d['idStoreProduct']==0 ) {
            
            $APIResponse['ERROR']=sprintf("Missing POST Information");            
            $APIResponse['RESPONSE']=4004; //MALLS MISSING QUERY DATA
            
            return $APIResponse;
        }
            
            
        $SQL=sprintf("UPDATE `storesProducts` `a`
                       SET
                       `active` = '%s'
                       WHERE `a`.`idUser` = '%s'
                       AND `a`.`idStoreProduct` = '%s'",   
                       $d['active'],
                       $d['idUser'],
                       $d['idStoreProduct']);

       #echo $SQL;die();
       
       $result = $this->_PDO->prepare($SQL); 
    
        if (!$result->execute()) {
            $APIResponse['ERROR']=sprintf("Error in query");            
            $APIResponse['RESPONSE']=4003; 
            return $APIResponse;
            
        }
        
        return $APIResponse;         
            
    }
    
    public function productsRemove($data=array()) 
	{
        # INIT REST API : begin
        $APIResponse=array();
        $APIResponse['RESPONSE']=1; //DEFAULT FALSE
        $APIResponse['ERROR']='No errors';  //hmm defaults..
        # INIT REST API : end
        
        # Data Validation 
        $d=array();
        $d['idUser']=is_numeric($this->_idUser)?$this->_idUser:0;
        $d['idStoreProduct']=is_numeric($data['idStoreProduct'])?$data['idStoreProduct']:0;        
        
        
        if ($d['idUser']==0 
            || $d['idStoreProduct']==0 ) {
            
            $APIResponse['ERROR']=sprintf("Missing POST Information");            
            $APIResponse['RESPONSE']=4004; //MALLS MISSING QUERY DATA
            
            return $APIResponse;
        }
        
        if (!$this->delete($d)) {
            $APIResponse['ERROR']=sprintf("Error in query");
            
            $APIResponse['RESPONSE']=3003; //MALLS MISSING QUERY DATA                                        
        }
        
        return $APIResponse;              
    }
    //PRIVATE Functions : begin
    private function productsGetCategories($data) {
        $categories="";        
        $cats=explode(";",$data);
        foreach ($cats as $cat) {
            $SQL=sprintf("SELECT `name`
                        FROM `storesProductsCategories` `a`
                        WHERE `a`.`idSPCategory`=%s",                    
                         $cat);
            
            $query = $this->_PDO->prepare($SQL);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);            
            $categories.=$row['name'].",";
        }        

        return substr_replace($categories, "", -1);
    }
    private function productsGetImages($data) {
        $imageLinks=array();
        
        $images=explode(";",$data);
        $i=0;
        if (is_array($images)) {
            foreach ($images as $image) {
                $SQL=sprintf("SELECT `mediaDescription`
                            FROM `media`
                            WHERE `media`.`idMedia`=%s",                    
                             $image);
                
                $query = $this->_PDO->prepare($SQL);
                $query->execute();
                $row = $query->fetch(PDO::FETCH_ASSOC);            
                $imageLinks[$i]['mediaUrl']=$this->_mediaUrl.$row['mediaDescription'];
                $imageLinks[$i]['idMedia']=$image;
                $i++;
            }            
        }
        else if (is_numeric($images)) {
            $SQL=sprintf("SELECT `mediaDescription`
                        FROM `media`
                        WHERE `media`.`idMedia`=%s",                    
                         $images);
            
            $query = $this->_PDO->prepare($SQL);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_ASSOC);            
            $imageLinks[$i]['mediaUrl']=$this->_mediaUrl.$row['mediaDescription'];
        }
        
        return $imageLinks;

    }
    
    private function add($data) {        
        
        $SQL=sprintf("INSERT INTO `storesProducts`
                     (`idStoreProduct`, `idUser`, `idStore`, `idSPCategory`, `name`, `images`, `imagesbg`, `description`, `price`, `price_sale`, `sale_start_date`, `sale_end_date`, `active`)
                     VALUES (NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                     $data['idUser'],
                     $data['idStore'],
                     $data['idSPCategory'],
                     $data['name'],
                     $data['images'],
                     $data['imagesbg'],                     
                     $data['description'],
                     $data['price'],
                     $data['price_sale'],
                     $data['sale_start_date'],
                     $data['sale_end_date'],
                     $data['active']);

        #echo $SQL;die();
        $result = $this->_PDO->prepare($SQL);
        if ($result->execute()) {
            return true;        
        }
        else {
            return false;
        }
        
    }    
    
    private function edit($data) {
        
        $SQL=sprintf("UPDATE `storesProducts` `a`
                       SET
                       `idStore` = '%s',
                       `idSPCategory` = '%s',
                       `name` = '%s',
                       `images` = '%s',
                       `imagesbg` = '%s',                       
                       `description` = '%s',
                       `price` = '%s',
                       `price_sale` = '%s',
                       `sale_start_date` = '%s',
                       `sale_end_date` = '%s',
                       `active` = '%s'
                       WHERE `a`.`idUser` = '%s'
                       AND `a`.`idStoreProduct` = '%s'",
                       $data['idStore'],
                       $data['idSPCategory'],
                       $data['name'],
                       $data['images'],
                       $data['imagesbg'],                       
                       $data['description'],
                       $data['price'],
                       $data['price_sale'],
                       $data['sale_start_date'],
                       $data['sale_end_date'],
                       $data['active'],
                       $data['idUser'],
                       $data['idStoreProduct']);

       #echo $SQL;
       $result = $this->_PDO->prepare($SQL);

        if ($result->execute()) {
            return true;        
        }
        else {
            return false;
        }
        
        
    }    
    
    private function delete($data=array()) {
        $SQL=sprintf("DELETE FROM `storesProducts`
                     WHERE
                     `idUser`=%s
                     AND `idStoreProduct`=%s",
                     $data['idUser'],
                     $data['idStoreProduct']);
        
        $result = $this->_PDO->prepare($SQL);
        
        if ($result->execute()) {//DIDN'T FIND THAT ID....
            return true;            
        }
        else {
            return false;
        }
            
    //PRIVATE Functions : end
    }
}

?>