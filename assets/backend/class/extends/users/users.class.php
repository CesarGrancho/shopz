<?php

/*
 *******************************************************************************
 *                      USERS Class
 *******************************************************************************
 *      Author:     César Pinto
 *      Email:      cesar.grancho@live.com.pt
 *      Website:    https://www.linkedin.com/in/cesargrancho/
 *
 *      File:       users.class.php
 *      Version:    1.0.8
 *      Copyright:  (c) 2017 César Pinto
 *                  
 *      License:    MIT
 *
 *******************************************************************************
 *  VERSION HISTORY:
 *******************************************************************************
 *      v1.0.9 [5.6.2018] 
 *		- Permissions now can be initialized in constructor from method _permissionsUserGet (TO DO)
 *
 *      v1.0.8 [18.11.2017] 
 *		- Permission structure changes
 *      
 *      v1.0.0 [06.06.2014] 
 *		- Public Version Release
 *
 *******************************************************************************
 *  DESCRIPTION:
 *******************************************************************************
 *  The class is intended to simplificy user managment
 *
 *  
 *******************************************************************************
 *  Functions Tree:
 *******************************************************************************
 *  
 *  .PUBLIC
 *  - init
 *  - routing
 *  
 *  .PRIVATE
 *  - initSystem
 *  -- sessionInit
 *  -- headersInit
 *  -- timezoneInit
 *  
 *  - initApp
 *  -- databaseInit
 *  
 *******************************************************************************
 *  USAGE:
 *******************************************************************************
 *  
 *  
 *******************************************************************************

*/


class users  {
    
	public $_idUser;	
    public $_userData;
	public $_userRoute;

    public $__errorLog; // USED to PARSE & DISPLAY ERRORS
	
	#Private
	

    public $_PDO; #rework
    private $_AUX;

    public function __construct($data = array())
    {
        $this->_PDO=$data['db'];
        $this->_AUX=$data['aux'];

        $this->_idUser=$data['idUser'];	
		 
		$this->_userRoute=$this->permissionsCheck($data['userRoute']); 							
	}

    #PUBLIC FUNCTION : begin
	
	public function permissionsCheck($data) 
	{							
		
		if (!$this->_AUX->stringValidation($data)) return false;				
		
		$cRoute=explode(".",$data);		
		
		$section=$cRoute[0];
		$mode=$cRoute[1];
		
		$action=$this->_AUX->stringValidation($cRoute[2])?$cRoute[2]:"";	
		
		#can be UPGRADED to be user custom database Permissions		
		
		
		/* 
		
		TO DO - $this->_permissionsUserGet() returns array
		get permissions for user		
		*/

		#check user permissions and update "c" command
		
		$permissions=[
						[
						"section" => "users",
						"redirects" => "dashboard.admin", #default route when allowed
						"action" => [
							"default" => "login.admin", #send user here when not allowed
							"allow" => ["login","register","usersLogin"]
							]
						],
						[
						"section" => "mediaGallery",
						"redirects" => "mediaGallery.admin", #default route when allowed
						"action" => [
							"default" => "login.admin", #send user here when not allowed
							"allow" => []
							]
						],
						[
						"section" => "products",
						"redirects" => "products.admin", #default route when allowed
						"action" => [
							"default" => "login.admin", #send user here when not allowed
							"allow" => []
							]
						]						
					]; 		

		#find Index for section			
		$key = array_search($section, array_column($permissions, 'section'));							
			
			
		#check access to restricted section : begin
		if (is_numeric($key)) {										
			
			#user NOT LOGGED OR NO PERMISSIONS
			if (!$this->UserLogCheck($this->_idUser)) {
				#check if action is allowed				
				
				if ($this->_AUX->stringValidation($action) &&
				is_numeric(array_search($action, $permissions[$key]["action"]["allow"]))) return implode(".",$cRoute);				

				#rebuild allowed cRoute command if not allowed action
				$cRoute=$permissions[$key]["action"]["default"];						
					
				return $cRoute;	
					
			}

			#Allow access to action		
			if ($this->_AUX->stringValidation($action)) return implode(".",$cRoute);		
			
			#No ACTIONS : begin 
			
				#find Index for section			
				$key = array_search($section, array_column($permissions, 'section'));						
				
				#rebuild allowed cRoute command
				$cRoute=$permissions[$key]["redirects"];			
				
				return $cRoute;
			
			#No ACTIONS : end							
		}
		#check access to restricted section : end
	
		#Allow ROUTE
		return implode(".",$cRoute);
		
	}
			
	
	public function userExist($data = array()) 
	{
		$idUser=is_numeric($data['idUser'])?$data['idUser']:0;
		$email=isset($data['email'])?$data['email']:"";	
		$username=isset($data['username'])?$data['username']:"";	
	
		$queryParams=array();
		if (is_numeric($idUser)
			&& $idUser>0) {
			//CHECK IF USER EXIST WITH IDUSER
			$SQL="SELECT *
					FROM `users` `a`
					WHERE `idUser`=:idUser";                		

			$queryParams[":idUser"]=$idUser;
						 
			
		} elseif ((isset($mail) && !is_null($email))
				  || (isset($username) && !is_null($username))) {
			//CHECK IF USER EXIST WITH MAIL
			$SQL="SELECT *
					FROM `users` `a`
					WHERE `email`=:mail
					OR `username`=:username";		

			$queryParams[":mail"]=$mail;						 
			$queryParams[":username"]=$username;						 
			
		} else {
			$this->__errorLog.=sprintf("No values- Id USer: %s - Mail: %s");
			
			return false;
		}
		
		#echo $SQL;die();
		
		$result = $this->_PDO->prepare($SQL);
		$result->execute($queryParams);
		

		$this->__errorLog.=sprintf("CHECK QUERY: %s",$SQL);
		#echo $this->__errorLog;
		if ($result->rowCount()==1) {
			//ASSIGN INFO FROM USER
			return true;
		} else {
			return false;
		}
	}	

    public function login($data = array()) 
	{

        $email=$data['email'];
        $password=md5(preg_replace('/\s+/', '', $data['password']));		        
		
        //CHECK IF USER EXIST
        
        $SQL = "SELECT * FROM `users` 
				WHERE 
				`email` = :email 
				AND
				`password` = :password
				AND
				`status` = 2";                     
		
        $result = $this->_PDO->prepare($SQL);	
		
        $result->execute([":email"=>$email, ":password" => $password]);
        #$result->debugDumpParams();
        $this->__errorLog.=sprintf("Check user credentials: %s\n", $SQL);        

        //LOG USER
        if ($result->rowCount()==1) {//USER FOUND

            $data=$result->fetch(PDO::FETCH_ASSOC);
            
            $_SESSION['loginAtempt']=0;

            $_SESSION['loginSession']=sprintf("%s_%s",
                                          $data['idUser'],
                                          time());

            $_SESSION['_idUser']=$data['idUser'];            

            $this->__errorLog.=sprintf("VAR LOGIN SESSION: %s\n",$_SESSION['loginSession']);

            //UPDATE SESSION VARS AND DATABASE
            //$this->UserLogCheck()
            return true;

        } else {
            return false;
        }        
    }
    
    
    public function addDetailed($data = array())
    {
        
        $SQL=sprintf("INSERT INTO `usersDetails` (`idUserDetails`, `idUser`, `idMall`, `idStore`, `idMedia`, `nameFirst`, `nameLast`, `phone`, `address`, `city`, `zipCode`, `state`, `idAgeGroup`, `birthday`, `idUserTag`)
                     VALUES (NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                     $data['idUser'],
                     $data['idMall'],
                     $data['idStore'],
                     $data['idMedia'],
                     $data['nameFirst'],
                     $data['nameLast'],
                     $data['phone'],
                     $data['address'],
                     $data['city'],
                     $data['zipCode'],
                     $data['state'],
                     $data['idAgeGroup'],
                     $data['birthday'],
                     $data['idUserTag']                     
                     );

        #echo $SQL;die();
        $result = $this->_PDO->prepare($SQL);
        
        if (!$result->execute()) {return false;}

        $this->__errorLog.=sprintf("INSERT NEW USER DETAILS: %s",$SQL);            
        return true;        
        
    }
    public function add($data = array())
    {

        $SQL=sprintf("INSERT INTO `users` (`idUser`, `idGroup`, `username`, `email`, `password`, `status`, `registrationCode`, `registrationDate`)
                     VALUES (NULL, '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
                     $data['idGroup'],
                     $data['username'],
                     $data['email'],
                     md5($data['password']),
                     $data['status'],
                     $data['registrationCode'],
                     time());

        #echo $SQL;die();
        $result = $this->_PDO->prepare($SQL);
        
        if (!$result->execute()) {return false;}

        $this->_userData['idUser']=$this->_PDO->lastInsertId();
        $this->__errorLog.=sprintf("INSERT NEW USER: %s",$SQL);            
        return true;
    }    
    
    public function edit($data = array())
    {
        $SQL=sprintf("UPDATE `users`
                       SET
                       `idGroup` = '%s',
                       `username` = '%s',
                       `email` = '%s',
                       `status` = '%s',
                       `registrationCode` = '%s'
                       WHERE `idUser` = '%s'",
                       $data['idGroup'],
                       $data['username'],
                       $data['email'],
                       $data['status'],
                       $data['registrationCode'],
                       $data['idUser']);

       #echo $SQL;die();
       $result = $this->_PDO->prepare($SQL);

        if ($result->execute()) {
            return true;        
        }
        else {
            return false;
        }        
        

    }
    public function editDetails($data = array())
    {
        $SQL=sprintf("UPDATE `usersDetails`
                       SET
                       `idMall` = '%s',
                       `idStore` = '%s',
                       `idMedia` = '%s',
                       `nameFirst` = '%s',
                       `nameLast` = '%s',
                       `phone` = '%s',
                       `address` = '%s',
                       `city` = '%s',
                       `zipCode` = '%s',
                       `state` = '%s',
                       `idAgeGroup`= '%s',
                       `birthday`= '%s',
                       `idUserTag`= '%s'
                       WHERE `idUser` = '%s'",
                       $data['idMall'],
                       $data['idStore'],
                       $data['idMedia'],
                       $data['nameFirst'],
                       $data['nameLast'],
                       $data['phone'],
                       $data['address'],
                       $data['city'],
                       $data['zipCode'],
                       $data['state'],
                       $data['idAgeGroup'],
                       $data['birthday'],
                       $data['idUserTag'],
                       $data['idUser']                       
                       );

       #echo $SQL;die();
       $result = $this->_PDO->prepare($SQL);

        if ($result->execute()) {
            return true;        
        }
        else {
            return false;
        }   
    }    
    public function passwordUpdate($data = array()) 
	{
        $SQL=sprintf("UPDATE `users`
                       SET
                       `password` = '%s'
                       WHERE `idUser` = '%s'",
                       md5($data['password']),
                       $data['idUser']);

       #echo $SQL;die();
       $result = $this->_PDO->prepare($SQL);

        if ($result->execute()) {
            return true;        
        }
        else {
            return false;
        }        
    }
    public function remove($data = array()) 
	{
        $SQL=sprintf("DELETE FROM `users`
                     WHERE
                     `idUser`=%s",
                     $data['idUser']);
        
        $result = $this->_PDO->prepare($SQL);
        
        if ($result->execute()) {//DIDN'T FIND THAT ID....
            return true;            
        }
        else {
            return false;
        }        
    }
    #PRIVATE FUNCTION : end



    #RE-WORK     



    public function UserLogCheck($idUser = 0)    {
        
        if (is_numeric($idUser)) {

            //retrieve user info && check if exist
            $userData=array();
            $userData['idUser']=$idUser;            
            
            if ($this->userExist($userData)) {
                //UPDATE SESSION VARS
                $_SESSION['loginSession']=sprintf("%s_%s",
                                  $idUser,
                                  time());

                $this->__errorLog.=sprintf("Update Session Vars: %s\n",$_SESSION['loginSession']);

                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }

    }

    public function PasswordReset($dataRequest)
    {
        //USAGE FOR GLOBAL 
        $this->_mail=$dataRequest['email'];
        
        $userMail=$this->_mail;
        
        //CHECK IF USER EXIST
        $SQL=sprintf("SELECT `idUser` FROM `users`
                     WHERE `mail` = '%s'
                     AND `status` IN (0,1)",
                     $userMail);

        $result = $this->_PDO->prepare($SQL);
        $result->execute();
        $this->__errorLog.=sprintf("CHECK IF USER MAIL EXIST :::: %s <br>",$SQL);


        //CHECK IF USER MAIL EXIST
        if ($result->rowCount()==1)
        {//RESET PASSWORD

            $userData=$result->fetch(PDO::FETCH_ASSOC);

            //generate random password
            //global usage for return inside object
            $this->_password=mt_rand(10300100, 59939139);

            $SQL=sprintf("UPDATE `users`
                         SET `password`='%s'
                         WHERE `idUser`='%s'",
                         md5($this->_password),
                         $userData['idUser']);

            $result = $this->_PDO->prepare($SQL);
            $result->execute();

            $this->__errorLog.=sprintf("UPDATE USER PASSWORD :::: %s <br>",$SQL);

            //echo $this->__errorLog;
            return true;
        }
        else
        {//TRIGGER - MAIL NOT FOUND
            return false;
        }
    }

    public function getUserInfo()
    {
        //GET ALL CLIENTS
        $SQL=sprintf("SELECT *, `users`.`idUser` as `idUserUnique` FROM `users`
                     LEFT JOIN `usersdetails`
                     ON `users`.`idUser` = `usersdetails`.`idUser`
                     WHERE `users`.`idUser`=%s
                     LIMIT 0,1",
                      $this->_idUser);

        $result = $this->_PDO->prepare($SQL);
        $result->execute();

        if ($result->rowCount()==0)
        {
            die;
        }

        // $result=mysql_query($SQL,$this->_DBCON);
        $result = $this->_PDO->prepare($SQL);
        $result->execute();

        $Data = array();

        $Data[]=$result->fetch(PDO::FETCH_ASSOC);
        
        //REMOVE SENSITIVE DATA
        unset($Data[0]['password']);
        unset($Data[0]['idUser']);
        
        //CHECK FOR NULL DATA
        if (is_null($Data[0]['country']))
        {
            $Data[0]['country']='';
        }
        
        if (is_null($Data[0]['timezone']))
        {
            $Data[0]['timezone']='0';
        }
        
        $this->__errorLog.=sprintf("GET USER INFORMATION: %s \n",$SQL);
        //echo $this->__errorLog;

        return $Data;
    }
    
    public function checkPassword($data){
        $password=$data['oldPassword'];
        $idUser=$data['idUser'];
        
        //CHECK IF USER EXIST
        $SQL=sprintf("SELECT * FROM `users`
                     WHERE `idUser` = '%s'
                     AND `password` = '%s'
                     AND `status` = 1",
                     $idUser,
                     md5(preg_replace('/\s+/', '', $password)));


        // $result=mysql_query($SQL,$this->_DBCON);
        $result = $this->_PDO->prepare($SQL);
        $result->execute();
        $this->__errorLog.=sprintf("CHECK IF PASSWORD IS CORRECT BY IDUSER: %s - %s\n",
                                   $SQL,
                                   $password);

        //LOG USER
        if ($result->rowCount()==1) {
            return true;   
        }
        else {
            return false;
        }
    }

    public function mailValid($data) {
        $mail=$data['oldMail'];
        $idUser=$data['idUser'];
        
        //CHECK IF USER EXIST
        $SQL=sprintf("SELECT * FROM `users`
                     WHERE `idUser` <> '%s'
                     AND `mail` = '%s'",
                     $idUser,
                     $mail);


        // $result=mysql_query($SQL,$this->_DBCON);
        $result = $this->_PDO->prepare($SQL);
        $result->execute();
        $this->__errorLog.=sprintf("CHECK IF MAIL IS USED BY ANOTHER USER: %s - %s\n",
                                   $SQL,
                                   $mail);

        
        if ($result->rowCount()==0) {
            
            //SEARCH FOR MEMBERS MAIL
            $SQL=sprintf("SELECT * FROM `members`
                         WHERE `mail` = '%s'",
                         $mail);
    
    
            // $result=mysql_query($SQL,$this->_DBCON);
            $result = $this->_PDO->prepare($SQL);
            $result->execute();
            $this->__errorLog.=sprintf("CHECK IF MAIL IS USED BY MEMBER: %s - %s\n",
                                       $SQL,
                                       $password);
            
            if ($result->rowCount()==0) {//NO MAIL FOUND IN USERS OR IN MEMBERS - VALID
                return true;
            }
            else {//INVALID MAIL
                return false;
            }
        }
        else {//INVALID MAIL
            return false;
        }        
    }

    // MAINTENANCE OPERATION : begin
    
    // MAINTENANCE OPERATION : end

}

?>
