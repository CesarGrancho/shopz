<?php

#Init User Variable - can be override by admin
$_idUser=$this->_USERS->_idUser;

#fileManager CLASS INITIALIZATION
$initfileManagerData=[
	"db" => $this->_DBCON,
	"aux" => new auxFunctions(),	
	"idUser" => $_idUser,
	"defaultFileUrl" => "./assets/frontend/views/admin/_files/"
];


#API CLASS INITIALIZATION
$initData=[
	"devMode" => $this->_systemData['devMode'],
	"idUser" => $_idUser,
	"aux" => new auxFunctions(), // CORE Auxiliar Functions
	"output" => "json", // TYPE OF OUTPUT
	"request" => $_REQUEST, // Handle the Current User Request
	"fileManager" => new fileManager($initfileManagerData) // Init Class Into API
];



#API CALL : begin
$api= new class($initData) {
	//core API
	private $_devMode;
	private $_AUX;
	private $_REQUEST;
	private $_OUTPUT;
	
	//idUser
	private $_idUser;
	
	//class env
	private $_fileManager;

	
	#CORE API Functions : begin	
	public function __construct($data=array()) {	
		$this->_devMode = $data["devMode"];	
		$this->_AUX = $data["aux"];	
		$this->_REQUEST = $data["request"];
		$this->_OUTPUT = $data["output"];
		
		
		$this->_idUser= $data["idUser"];
						
		
		$this->_fileManager= $data["fileManager"];
	}			
	
	public function apiError($data=array()) {
		$APIResponse["ERROR"]="API Error - Nothing to display";
		$APIResponse["RESPONSE"]="25"; 
		
		return $APIResponse;	
	}
	
	public function output($data=array()) {
	
		switch ($this->_OUTPUT) {
			
			default:
			#json
			return json_encode($data);
		}
		
	}
	#CORE API Functions : end 
	
	
	public function imagesGet($data=array()) 
	{        
        /* INIT REST API RETURN OBJECT */
		$API = (object) array(
					"RESPONSE" => 1,
					"ERROR" => 'No errors',
					"__errorLog" => "",
					"results" => 0,
					"data" => array()
		);			   				
		
		//check how many images user has : begin		
			if (!is_numeric($API->results=$this->_fileManager->fileExist([
							'idUser'=> $this->idUser,
							'fileType' => 1 //images
						])))
				{ //NOT Numeric
				$API->RESPONSE=25; //Error
				$API->ERROR='Error fetching number of total images.';
				$API->__errorLog=($this->_devMode?$this->_fileManager->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
				return $API;  
			}						
			
			//return if user has no images
			if ($API->results==0) return $API;
		//check how many images user has : end
		
		//get user images : begin		
			if (!is_array($API->data=$this->_fileManager->fileGet([
							'idUser'=> $this->_idUser,
							'fileType' => 1 //images
						])))
				{ //NOT Numeric
				$API->RESPONSE=25; //Error
				$API->ERROR='Error fetching images.';
				$API->__errorLog=($this->_devMode?$this->_fileManager->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
				return $API;  
			}	
		//get user images : end
		
		#Return API Answer
		$API->__errorLog=($this->_devMode?$this->_fileManager->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
		return $API; 	
	}
		
	public function imagesDelete($data=array()) {
        /* INIT REST API RETURN OBJECT */
		$API = (object) array(
					"RESPONSE" => 1,
					"ERROR" => 'Success',
					"__errorLog" => ""

		);			
		
		#check array, cycle array, check if image exist, delete : begin
			$fileItems=json_decode($data['fileItems']);
			
			if (!is_array($fileItems)) {
				$API->RESPONSE=25; //Error
				$API->ERROR='Error in File Items: ' . $data['idFile'];
				$API->__errorLog=($this->_devMode?"API Validation":""); // EXPORT ERROR IF IN DEV MODE
				return $API;				
			}
			
			
			foreach ($fileItems as $idFile) {		
				$fileData=[
								'idUser'=> $this->_idUser,
								'idFile' => $idFile
							];
				if (!is_numeric($API->results=$this->_fileManager->fileExist($fileData)))
					{ //NOT valid query
					$API->RESPONSE=25; //Error
					$API->ERROR='Error fetching image.';
					$API->__errorLog=($this->_devMode?$this->_fileManager->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
					return $API;  
				}			
				
				#return if image wasn't found
				if ($API->results==0) {
					$API->RESPONSE=25; //Error
					$API->ERROR='Image not found.';
					$API->__errorLog=($this->_devMode?$this->_fileManager->__errorLog:""); // EXPORT ERROR IF IN DEV MODE				
					return $API;
				}
				
				#delete image 
				if (!$this->_fileManager->fileDelete($fileData)) {
					$API->RESPONSE=25; //Error
					$API->ERROR='Error while removing image';
					$API->__errorLog=($this->_devMode?$this->_fileManager->__errorLog:""); // EXPORT ERROR IF IN DEV MODE				
					return $API;
				}				
		
				
			}
		#cycle in array, check if image exist, delete : end
		

		
		#Return API Answer
		$API->__errorLog=($this->_devMode?$this->_fileManager->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
		return $API; 			
	}

	
	public function imageUpload($data=array()) {
        /* INIT REST API RETURN OBJECT */
		$API = (object) array(
					"RESPONSE" => 1,
					"ERROR" => 'Success',
					"__errorLog" => ""
		);			
		
		$filesData=[
			"fileType" => 1, #images
			"files" => $data["files"],
			"idUser" => $this->_idUser
			];
		
		if (!$this->_fileManager->fileUpload($filesData)) {   				
			$API->RESPONSE=25; //Error
			$API->ERROR="Error uploading files";
			$API->__errorLog=($this->_devMode?$this->_fileManager->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
			return $API;								
		}
		
		#Return API Answer
		$API->__errorLog=($this->_devMode?$this->_fileManager->__errorLog:""); // EXPORT ERROR IF IN DEV MODE
		return $API; 		
	}
};
	
#API CALL : end


#api method exist?
if (!method_exists($api,$this->_APIActions['actions'])) {
	
	echo $api->output($api->apiError());	
	exit();
}

#call dynamic method
$_REQUEST["files"]=$_FILES; #add file uploads
echo $api->output($api->{$this->_APIActions['actions']}($_REQUEST));

?>
