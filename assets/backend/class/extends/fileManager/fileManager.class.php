<?php

/*******************************************************************************
 *                      File Manager Class
 *******************************************************************************
 *      Author:     César Pinto
 *      Email:      cesar.grancho@live.com.pt
 *      Website:    https://www.linkedin.com/in/cesargrancho/
 *
 *      File:       fileManager.class.php
 *      Version:    1.0.7
 *      Copyright:  (c) 2018 César Pinto
 *                  
 *      License:    MIT
 *
 *******************************************************************************
 *  VERION HISTORY:
 *******************************************************************************
 *      v1.0.7 [22.07.2018] - changed from Media Gallery into File Manager 
							( improved code abstraction )
 *
 *      v1.0.6 [17.07.2018] - Refactoring code
 *
 *      v1.0.0 [2.06.2017] - Initial Version
 *
 *******************************************************************************
 *  DESCRIPTION:
 *******************************************************************************
 *  The class got functions for file gallery operations
 *
 *   -] HOW USE [-
 *******************************************************************************
*/



class fileManager  {
    
    public $_idFile;
    
    public $_fileData; //object array with database fields
	
	public $__errorLog;
	
	public $_defaultFileUrl;
	public $_defaultUserFileUrl;
		
    private $_idUser;    
    private $_PDO; //DATABASE OBJECT			
	private $_AUX; //AUXILIAR FUNCTIONS OBJECT
    
    public function __construct($data=array())
    {		
		
        $this->_PDO=$data["db"];
		$this->_AUX=$data["aux"];
		
		$this->_fileData = (object) array(
			"_idFile" => 0,
			"_fileDescription" => "",
			"_fileUrl" => "", //baseURL/_files/fileName
			"_fileType" => 0, //1: IMAGE - others
			"_fileTags" => "",
		);		    
        
        $this->_idUser=isset($data["idUser"])?$data["idUser"]:0;	
        
        $this->_defaultFileUrl=isset($data["defaultFileUrl"])?$data["defaultFileUrl"]:'./assets/_files/';//default FILES FOLDER  				

		$this->_defaultUserFileUrl=sprintf($this->_defaultFileUrl.'%s/',$this->_idUser); //default USER FILES FOLDER 
		
		$this->__errorLog="";
        
        $this->fileManagerMaintenance(["_defaultFileUrl" => $this->_defaultFileUrl]);
    }
    
    // PUBLIC FUNCTIONS : begin 
	public function fileExist($data = array()) 
	{
		# can check how many file items user has when fileType is passed
		# can check if file exist by idFile
		# requires: 
		#		idUser
		#
		# conditional:
		#		fileType 
		#		idFile
		#		fileName
		#
		# returns:
		# 		false 
		#		integer
		
		$this->__errorLog.=("<br>\n fileExist(init)");	
		
		//INIT Class Variables : begin
			$SQLCondition="";
			
			#idUser validation : begin
				$idUser=isset($data["idUser"])?$data["idUser"]:$this->_idUser;        
			
				#allow only numeric values
				if (!is_numeric($idUser)|| $idUser==0) {
					$this->__errorLog.=sprintf("->(error in idUser: %s)", $idUser);
					return false; 
				}				
				
				$SQLQueryData[":idUser"]=$idUser;	
			#idUser validation : end
			
			#fileType validation : begin
				$fileType=isset($data["fileType"])?$data["fileType"]:0;        
				
				#allow only numeric values
				if (!is_numeric($fileType)) {
					$this->__errorLog.=sprintf("->(error in FileType: %s)",$fileType);
					return false; 
				}		
				
				#ADD SQL & query array
				if ($fileType>0) {
					$SQLQueryData[":fileType"]=$fileType;	
					#Exclusive Condition Search Remove Previous SQL					
					$SQLCondition="AND `a`.`fileType`=:fileType";								
				}			
			#fileType validation : end
			
			#idFile validation : begin
				$idFile=isset($data["idFile"])?$data["idFile"]:0;        
				
				#allow only numeric values
				if (!is_numeric($idFile)) {
					$this->__errorLog.=sprintf("->(error in idFile: %s)",$idFile);
					return false; 
				}		
				
				#ADD SQL & query array
				if ($idFile>0) {
					$SQLQueryData[":idFile"]=$idFile;			
					#Exclusive Condition Search Remove Previous SQL
					$SQLCondition=" AND `a`.`idFile`=:idFile";								
				}				
			#idFile validation : end
			
			#fileName validation : begin
				$fileName=isset($data["fileName"])?$data["fileName"]:"";        
				
				#ADD SQL & query array
				if ($fileName!="") {
					$SQLQueryData[":fileName"]=$fileName;				
					$SQLCondition.=" AND `a`.`fileName`=:fileName";								
				}				
			#fileName validation : end			
			
		//INIT Class Variables : end

		//CHECK IF USER HAS FILES : begin
			$SQL=sprintf("SELECT count(*) as `results` FROM `fileManager` `a`
						 WHERE `a`.`idUser`=:idUser
						 %s
						 ORDER BY `a`.`idFile` DESC",
						 $SQLCondition
						 );	

						
			#echo $this->_AUX->PDODebugger($SQL,$SQLQueryData);														
			$resultQuery = $this->_PDO->prepare($SQL);	
			
			if (!$resultQuery->execute($SQLQueryData)) {
				$this->__errorLog.=sprintf("->(error in query to user file)[ %s ]",$this->_AUX->PDODebugger($SQLQuery,$SQLQueryData));
				return false;
			}	
			$d=$resultQuery->fetch(PDO::FETCH_ASSOC);
		//CHECK IF USER HAS FILES : end

		
		return $d["results"];
		
	}
	
	public function fileGet($data=array()) 
	{
		# get file items for user
		# requires: 
		#		idUser
		#
		# conditional:
		#		fileType 
		#		idFile		
		#
		# returns:
		# 	false (on error) 
		#	array
		
		$this->__errorLog.=("<br>\n fileGet(init)");	

		//INIT Class Variables : begin			
			$SQLCondition="";
			
			#idUser validation : begin
				$idUser=isset($data["idUser"])?$data["idUser"]:$this->_idUser;        
				
				#allow only numeric values
					if (!is_numeric($idUser)|| $idUser==0) {
						$this->__errorLog.=sprintf("->(error in idUser: %s)", $idUser);
						return false; 
					}				
					
					$SQLQueryData[":idUser"]=$idUser;	
			#idUser validation : end	

			
			#fileType validation : begin
				$fileType=isset($data["fileType"])?$data["fileType"]:0;        
				
				#allow only numeric values
				if (!is_numeric($fileType)) {
					$this->__errorLog.=sprintf("->(error in FileType: %s)",$fileType);
					return false; 
				}		
				
				#ADD SQL & query array
				if ($fileType>0) {
					$SQLQueryData[":fileType"]=$fileType;				
					$SQLCondition.="AND `a`.`fileType`=:fileType";								
				}			
			#fileType validation : end
			
			#idFile validation : begin
				$idFile=isset($data["idFile"])?$data["idFile"]:0;        
				
				#allow only numeric values
				if (!is_numeric($idFile)) {
					$this->__errorLog.=sprintf("->(error in idFile: %s)",$idFile);
					return false; 
				}		
				
				#ADD SQL & query array
				if ($idFile>0) {
					$SQLQueryData[":idFile"]=$idFile;				
					$SQLCondition.="AND `a`.`idFile`=:idFile";								
				}				
			#idFile validation : end
			
		//INIT Class Variables : end	
		
		#GET USER FILES : begin
			#create sql 
			$SQL=sprintf("SELECT * FROM `fileManager` `a`
						 WHERE `a`.`idUser`=:idUser
						 %s
						 ORDER BY `a`.`idFile` DESC",
						 $SQLCondition
						 );		

						
			//echo $this->_AUX->PDODebugger($SQL,$SQLQueryData);die();																
			$resultQuery = $this->_PDO->prepare($SQL);	
			
			#try get data from database
			if (!$resultQuery->execute($SQLQueryData)) {
				$this->__errorLog.=sprintf("->(error in query to user file)[ %s ]",$this->_AUX->PDODebugger($SQLQuery,$SQLQueryData));
				return false;
			}	
			
			#if has no records return empty array
			$d=array();
			if ($resultQuery->rowCount()==0) {
				$this->__errorLog.=("->(no file found for user)");
				return $d;
			}
			
			#prepare file data array			
            $i =0;
            while ($row = $resultQuery->fetch(PDO::FETCH_ASSOC)) {
                $d[$i]=$row;
                $d[$i]["fileUrl"]=$this->_defaultUserFileUrl.$row["fileName"];
                $i++;
            };			
		#CHECK IF USER HAS FILES : end		
		
		return $d;				
	}	
	
	public function fileDelete($data=array()) 
	{
		# can check how many file items user has when fileType is passed
		# can check if file exist by idFile
		# requires: 
		#		idUser
		#		idFile
		#
		# returns:
		# 		true/false 

		
		$this->__errorLog.=("<br>\n fileDelete(init)");	
		
		#INIT Class Variables : begin
			$SQLCondition="";
			
			#idUser validation : begin
			$idUser=isset($data["idUser"])?$data["idUser"]:$this->_idUser;        
			
			#allow only numeric values
				if (!is_numeric($idUser)|| $idUser==0) {
					$this->__errorLog.=sprintf("->(error in idUser: %s)", $idUser);
					return false; 
				}				
				
				$SQLQueryData[":idUser"]=$idUser;	
			#idUser validation : end
			
			#user file url			
			$_fileUrl=sprintf($this->_defaultFileUrl.'%s/',$idUser);
			
			#idFile validation : begin
				$idFile=isset($data["idFile"])?$data["idFile"]:0;        
				
				#allow only numeric values
				if (!is_numeric($idFile)) {
					$this->__errorLog.=sprintf("->(error in idFile: %s)",$idFile);
					return false; 
				}		
				
				#ADD SQL & query array
				if ($idFile>0) {
					$SQLQueryData[":idFile"]=$idFile;				
					$SQLCondition.="AND `idFile`=:idFile";								
				}				
			#idFile validation : end
			
		#INIT Class Variables : end	
		
		#get file item details
		if (!is_array($fileItem=$this->fileGet(["idUser"=>$idUser,"idFile"=>$idFile]))) {
			$this->__errorLog.="->(no file item found)";
			return false;
		}			
		
		
		#delete file : begin		
			
			#delete from disk : begin
				#check if file exist
				if (!file_exists($fileItem[0]['fileUrl'])) {
					$this->__errorLog.="->(file item not found in disk)";
					return false;				
				}			
				
				#try delete file
				if (!unlink ($fileItem[0]['fileUrl'])) {
					$this->__errorLog.="->(error while deleting file item from disk)";
					return false;								
				}			
			#delete from disk : end			
			
			
			#delete from database : begin 			
                $SQLQuery=sprintf("DELETE FROM `fileManager`
                             WHERE
                             `idUser`=:idUser
                             %s",
                             $SQLCondition);
                
				#echo $this->_AUX->PDODebugger($SQLQuery,$SQLQueryData);die();																
				$SQLResult = $this->_PDO->prepare($SQLQuery);
			
				if (!$SQLResult->execute($SQLQueryData)) {
					$this->__errorLog.=sprintf("->(Error deleting file item from Database)[ %s ]",$this->_AUX->PDODebugger($SQLQuery,$SQLQueryData));
					return false;          
				}		
			#delete from database : end
		
		#delete file : end
		
		return true;

	}

    public function fileAdd($data=array()) 
	{
		/*
		 add file items for user
		 requires: 
				idUser
				fileName
				fileType
		
		 conditional:
				fileDescription 
				fileTags		
		
		 returns:
		 	false (on error) 
			idFile (new index)		
		*/
		
		$this->__errorLog.=("<br>\n fileAdd(init)");		
		
		#INIT Class Variables : begin
			$SQLCondition="";
			
			#idUser validation : begin
				$idUser=isset($data["idUser"])?$data["idUser"]:$this->_idUser;        
			
				#allow only numeric values
				if (!is_numeric($idUser)|| $idUser==0) {
					$this->__errorLog.=sprintf("->(error in idUser: %s)", $idUser);
					return false; 
				}				
			#idUser validation : end
			
			#fileName validation : begin			
				$fileName=isset($data["fileName"])?$data["fileName"]:0;        
				
				#allow only numeric values
				if ($fileName=="") {
					$this->__errorLog.=sprintf("->(error in fileName: %s)",$fileName);
					return false; 
				}					
				

			#fileName validation : end
			
			#fileType validation : begin
				$fileType=isset($data["fileType"])?$data["fileType"]:0;        
				
				#allow only numeric values
				if (!is_numeric($fileType) || $fileType==0) {
					$this->__errorLog.=sprintf("->(error in fileType: %s)",$fileType);
					return false; 
				}		
			#fileType validation : end
			
			#create data array
			$SQLQueryData=[
				":idUser" => $idUser,
				":fileName" => $fileName,
				":fileType"=> $fileType,
				":fileDescription" => $data["fileDescription"],
				":fileTags" => $data["fileTags"]
			];
			
		#INIT Class Variables : end	
		
		
		#insert into database : begin
            $SQLQuery="INSERT INTO `fileManager`
                         (`idFile`, `idUser`, `fileDescription`, `fileName`, `fileType`, `fileTags`)
                         VALUES (NULL, :idUser, :fileDescription, :fileName, :fileType, :fileTags)";

            #echo $this->_AUX->PDODebugger($SQLQuery,$SQLQueryData);die();																
            $SQLResult = $this->_PDO->prepare($SQLQuery);
			
			if (!$SQLResult->execute($SQLQueryData)) {
				$this->__errorLog.=sprintf("->(Error insert file item to Database)[ %s ]",$this->_AUX->PDODebugger($SQLQuery,$SQLQueryData));
				return false;          
			}				
            	
		#insert into database : end		
		
		return $this->_PDO->lastInsertId();
		
	}

	public function fileUpload($data=array()) {
		# upload file items for user
		# requires: 
		#		idUser
		#		files ( $_FILES )
		#		fileType 1: images
		#
		#
		# returns:
		# 	false (on error) 
		#	true
		$this->__errorLog.=("<br>\n fileUpload(init)");		
				
		#INIT Class Variables : begin
			$SQLCondition="";
			
			#idUser validation : begin
				$idUser=isset($data["idUser"])?$data["idUser"]:$this->_idUser;        
			
				#allow only numeric values
				if (!is_numeric($idUser)|| $idUser==0) {
					$this->__errorLog.=sprintf("->(error in idUser: %s)", $idUser);
					return false; 
				}				
			#idUser validation : end
			
			#fileType validation : begin
				$fileType=isset($data["fileType"])?$data["fileType"]:0;        
				
				#allow only numeric values
				if (!is_numeric($fileType) || $fileType==0) {
					$this->__errorLog.=sprintf("->(error in fileType: %s)",$fileType);
					return false; 
				}		
				
				#get File Mime Types
				if (!is_array(($allowedMimeTypes=$this->fileMimeTypeGet(["fileType"=>$fileType])))) {
					$this->__errorLog.=sprintf("->(error in getting allowed Mime Type: %s)",$fileType);
					return false; 
				}			

				#check if has files are valid
				$files=$data["files"];
				if (!empty($files) 
					&& !is_array($files['file']['tmp_name'])           
					&& count($files['file']['tmp_name'])==0) {   	
					$this->__errorLog.=sprintf("->(error in uploaded files: %s)",$files);
					return false;
				}				
					
			#fileType validation : end		
			
		#INIT Class Variables : end			

		#cycle files(check if filename exist)->(add) : begin
			
			#cycle files : begin 
            $file=0;

            do {                    
				#INIT ARRAY FILE Variable 
				$fileData=array();					
				
				#check if file type is allowed : begin 						
					if (!in_array($files['file']['type'][$file], $allowedMimeTypes)) {
						$this->__errorLog.=sprintf("->(File type not allowed: %s)",$files['file']['type'][$file]);
						return false;
					}				
					#assign media type "image"
					$fileData['fileType']=$fileType; 
				#check if file type is allowed : end
				
				#create unique filename : begin
				
					#reset counter for new filename
					$limit=0; 
					
					#assign filename
					$fileData['fileName']=$files['file']['name'][$file]; 
					
					#do while filename already exist                  
					while ($this->fileExist($fileData)==1) {					
						
						#split file name into array
						$fileNameSplit=explode(".",$fileData['fileName']);                    
						
						#get file extension
						$fileExtension=$fileNameSplit[(count($fileNameSplit)-1)];                    
						
						#remove extension
						array_pop($fileNameSplit); 
						
						#check if array or string
						if (is_array($fileNameSplit)) {                        
							#merge array splited "some.file.like.ext"
							$fileData['fileName']=implode('.', $fileNameSplit)."_.".$fileExtension;
							//echo "array :".$fileData['fileName'];
						}						
						else {
							#single string "filename.ext"
							$fileData['fileName']=$fileNameSplit."_.".$fileExtension;
							//echo "not array :".$fileData['fileName'];
						}						
						
						#check filename creations - MAX is 5
						if ($limit>5) {
							$this->__errorLog.=sprintf("->(Error creating Filename - Last tried: %s)",$fileData['name']);
							return false;														
						}
						$limit++;
					}     
				#create unique filename : end                
				
				#copy file to user files directory : begin		
					$targetFile = $this->_defaultUserFileUrl . $fileData['fileName'];               
					//echo $tempFile."  :::: ".$targetFile;  				
				
					if (!move_uploaded_file($files['file']['tmp_name'][$file],$targetFile)) {
						$this->__errorLog.=sprintf("->(Error moving file to user directory - Temporary Name: %s moving into %s)",$files['file']['tmp_name'][$file],$targetFile);
						return false;													
					}			
				#copy file to user files directory : end
				
				#add file into database  
				if (!is_numeric($this->fileAdd($fileData))) { 
					$this->__errorLog.="->(Error adding file to Database)";
					return false;						
				}				                

                
                $file++;
            } while ($file<count($files['file']['tmp_name'])); //go for multi files
			#cycle files : end		
			
		
		#cycle array, check if filename exist, add : begin
		
		
		return true;
	}	
	
	// PUBLIC FUNCTIONS : end
	
    // PRIVATE FUNCTIONS : begin 
    
    private function fileManagerMaintenance($data=array()) {
							 
		#echo $data['_defaultFileUrl'];die();
		$_fileUrl=$data['_defaultFileUrl'];        
        
		#check if file folder exist
        $userFolderExist = function($url) {
            //$url;
            if (!is_null($url)
                && !file_exists($url)) {
                mkdir($url, 0777, true);
                return true;
            }             
        };                
        
        $userFolderExist($_fileUrl);
        
    }
	
	
	private function fileMimeTypeGet($data=array()) {
		# check what are the Mime Types allowed for fileType 
		# requires: 
		#		fileType 1: images
		#
		#
		# returns:
		# 	false (on error) 
		#	array
		$this->__errorLog.=("<br>\n fileMimeTypeGet(init)");		
		
		#INIT Class Variables : begin
			
			#fileType validation : begin
				$fileType=isset($data["fileType"])?$data["fileType"]:0;        
			
				#allow only numeric values
				if (!is_numeric($fileType)|| $fileType==0) {
					$this->__errorLog.=sprintf("->(error in fileType: %s)", $fileType);
					return false; 
				}				
			#fileType validation : end	
			
		#INIT Class Variables : end
		
		if ($fileType==1) {
			return array(
				"image/gif",
				"image/png",
				"image/jpeg",
				"image/pjpeg"
			);	
		}
		
		$this->__errorLog.=sprintf("->(error in fileType: %s)", $data['fileType']);
		return false;
	}

	
	// PRIVATE FUNCTIONS : end
}


?>