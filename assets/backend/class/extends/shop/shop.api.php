<?php


#PRODUCTS CLASS INITIALIZATION
$initProductData=[
	"db" => $this->_DBCON,
	"aux" => new auxFunctions(),	
	"user" => $this->_idUser	
];


#API CLASS INITIALIZATION
$initData=[
	"aux" => new auxFunctions(),
	"output" => "json",
	"request" => $_REQUEST,
	"db" => $pandix->_DBCON
	"products" => new products($initProductData)
	"users" => $this->_USERS	
];



#API CALL : begin
$api= new class($initData) {

	private $_REQUEST;
	private $_AUX;
	
	#CORE API Functions : begin
	
	public function __construct($data=array()) {	
		$this->_REQUEST = $data["request"];
		$this->_AUX = $data["aux"];			
		
		$this->_USERS= $data["users"];
	}			
	
	public function apiError($data=array()) {
		$APIResponse["ERROR"]="API Error - Nothing to display";
		$APIResponse["RESPONSE"]="25"; 
		
		return $APIResponse;	
	}
	
	public function output($data=array()) {
	
		switch ($this->_OUTPUT) {
			
			default:
			#json
			return json_encode($data);
		}
		
	}
	
	#CORE API Functions : end 
	public function productsList
	
        /* SET GLOBALS VARS LOCAL */
        $_idUser=isset($this->_idUser)?$this->_idUser:0;
        $_idStoreProduct=isset($data['idStoreProduct'])?$data['idStoreProduct']:0;
        
        /* INIT REST API VAR */
        $response=array();
        $response['RESPONSE']=1; //DEFAULT FALSE
        $response['ERROR']='No errors';  //hmm defaults..    		
		
		
		if (!$this->addDetailed($d)) { //SOMETHING WENT WRONG ADDING NEW MALL                        
			$APIResponse['RESPONSE']=25; //Error
			$APIResponse['ERROR']='Error adding user details';
			return $APIResponse;  
		}
		
		return $APIResponse; 		
		
};
	
#API CALL : end


#api method exist?
if (!method_exists($api,$this->_APIActions['actions'])) {
	
	echo $api->output($api->apiError());	
	exit();
}

#call dynamic method
echo $api->output($api->{$this->_APIActions['actions']}($_REQUEST));

?>
