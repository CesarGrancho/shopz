-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 23, 2018 at 07:10 PM
-- Server version: 10.1.23-MariaDB-9+deb9u1
-- PHP Version: 7.0.27-0+deb9u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `project_shopz`
--

-- --------------------------------------------------------

--
-- Table structure for table `fileManager`
--

CREATE TABLE `fileManager` (
  `idFile` bigint(20) NOT NULL,
  `idUser` bigint(20) NOT NULL,
  `fileDescription` varchar(255) DEFAULT NULL,
  `fileName` text NOT NULL,
  `fileType` smallint(6) NOT NULL,
  `fileTags` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `idStoreProduct` bigint(20) NOT NULL,
  `idUser` bigint(20) NOT NULL,
  `idProductCategory` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `images` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `price` float NOT NULL,
  `price_sale` float NOT NULL,
  `sale_start_date` bigint(20) NOT NULL,
  `sale_end_date` bigint(20) NOT NULL,
  `active` smallint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `productsCategories`
--

CREATE TABLE `productsCategories` (
  `idProductCategory` bigint(20) NOT NULL,
  `idUser` bigint(20) NOT NULL,
  `productCatDescription` text NOT NULL,
  `productCatActive` smallint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `settingsCompanyInformation`
--

CREATE TABLE `settingsCompanyInformation` (
  `idSCI` bigint(20) NOT NULL,
  `SCIName` varchar(255) NOT NULL,
  `SCIAddress` text NOT NULL,
  `SCICity` varchar(255) NOT NULL,
  `SCIState` varchar(255) NOT NULL,
  `SCIZipcode` varchar(255) NOT NULL,
  `SCIPhone1` varchar(255) NOT NULL,
  `SCIPhone2` varchar(255) NOT NULL,
  `SCIEmail1` varchar(255) NOT NULL,
  `SCIEmail2` varchar(255) NOT NULL,
  `idMedia` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settingsPayments`
--

CREATE TABLE `settingsPayments` (
  `idSPayment` bigint(20) NOT NULL,
  `idSPDescription` varchar(255) NOT NULL,
  `SPOptions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settingsSMS`
--

CREATE TABLE `settingsSMS` (
  `idSSMS` bigint(20) NOT NULL,
  `SSMSDescription` varchar(255) NOT NULL,
  `SSMSOptions` text NOT NULL,
  `SSMSStatus` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settingsSMTP`
--

CREATE TABLE `settingsSMTP` (
  `idSSMTP` bigint(20) NOT NULL,
  `smtpURL` text NOT NULL,
  `smtpIdentity` varchar(255) NOT NULL,
  `smtpFromEmail` varchar(255) NOT NULL,
  `smtpUsername` varchar(255) NOT NULL,
  `smtpPassword` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settingsTaxes`
--

CREATE TABLE `settingsTaxes` (
  `idSTax` bigint(20) NOT NULL,
  `idSTType` smallint(6) NOT NULL,
  `STDescription` varchar(255) NOT NULL,
  `STValue` varchar(255) NOT NULL,
  `STStatus` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `idUser` bigint(20) NOT NULL,
  `idGroup` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `registrationCode` int(11) NOT NULL DEFAULT '0',
  `registrationDate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usersDetails`
--

CREATE TABLE `usersDetails` (
  `idUserDetails` bigint(20) NOT NULL,
  `idUser` bigint(20) NOT NULL,
  `idMall` bigint(20) NOT NULL DEFAULT '0',
  `idStore` bigint(20) NOT NULL DEFAULT '0',
  `idMedia` bigint(20) NOT NULL DEFAULT '0',
  `nameFirst` varchar(255) NOT NULL,
  `nameLast` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(255) NOT NULL,
  `zipCode` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `idAgeGroup` smallint(6) NOT NULL,
  `birthday` bigint(20) NOT NULL DEFAULT '0',
  `idUserTag` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usersTags`
--

CREATE TABLE `usersTags` (
  `idUserTag` bigint(20) NOT NULL,
  `tagName` varchar(255) NOT NULL,
  `tagDescription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fileManager`
--
ALTER TABLE `fileManager`
  ADD UNIQUE KEY `idMedia` (`idFile`),
  ADD KEY `idUser` (`idUser`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD UNIQUE KEY `ID` (`idStoreProduct`),
  ADD KEY `idUser` (`idUser`),
  ADD KEY `idSPCategory` (`idProductCategory`);

--
-- Indexes for table `productsCategories`
--
ALTER TABLE `productsCategories`
  ADD PRIMARY KEY (`idProductCategory`),
  ADD KEY `idUSer` (`idUser`);

--
-- Indexes for table `settingsCompanyInformation`
--
ALTER TABLE `settingsCompanyInformation`
  ADD PRIMARY KEY (`idSCI`),
  ADD UNIQUE KEY `idSCI` (`idSCI`),
  ADD KEY `idMedia` (`idMedia`);

--
-- Indexes for table `settingsPayments`
--
ALTER TABLE `settingsPayments`
  ADD PRIMARY KEY (`idSPayment`),
  ADD UNIQUE KEY `idSPayment` (`idSPayment`);

--
-- Indexes for table `settingsSMS`
--
ALTER TABLE `settingsSMS`
  ADD PRIMARY KEY (`idSSMS`),
  ADD UNIQUE KEY `idSSMS` (`idSSMS`);

--
-- Indexes for table `settingsSMTP`
--
ALTER TABLE `settingsSMTP`
  ADD PRIMARY KEY (`idSSMTP`),
  ADD UNIQUE KEY `idSSMTP` (`idSSMTP`);

--
-- Indexes for table `settingsTaxes`
--
ALTER TABLE `settingsTaxes`
  ADD PRIMARY KEY (`idSTax`),
  ADD UNIQUE KEY `idSTax` (`idSTax`),
  ADD KEY `idSTType` (`idSTType`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `ID` (`idUser`),
  ADD KEY `idGroup` (`idGroup`);

--
-- Indexes for table `usersDetails`
--
ALTER TABLE `usersDetails`
  ADD UNIQUE KEY `idUserDetails` (`idUserDetails`),
  ADD KEY `idUser` (`idUser`),
  ADD KEY `idMall` (`idMall`),
  ADD KEY `idStore` (`idStore`),
  ADD KEY `idAgeGroup` (`idAgeGroup`);

--
-- Indexes for table `usersTags`
--
ALTER TABLE `usersTags`
  ADD PRIMARY KEY (`idUserTag`),
  ADD UNIQUE KEY `idUserTag` (`idUserTag`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fileManager`
--
ALTER TABLE `fileManager`
  MODIFY `idFile` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `idStoreProduct` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `productsCategories`
--
ALTER TABLE `productsCategories`
  MODIFY `idProductCategory` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `settingsCompanyInformation`
--
ALTER TABLE `settingsCompanyInformation`
  MODIFY `idSCI` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settingsPayments`
--
ALTER TABLE `settingsPayments`
  MODIFY `idSPayment` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `settingsSMS`
--
ALTER TABLE `settingsSMS`
  MODIFY `idSSMS` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `settingsSMTP`
--
ALTER TABLE `settingsSMTP`
  MODIFY `idSSMTP` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `settingsTaxes`
--
ALTER TABLE `settingsTaxes`
  MODIFY `idSTax` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `idUser` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `usersDetails`
--
ALTER TABLE `usersDetails`
  MODIFY `idUserDetails` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `usersTags`
--
ALTER TABLE `usersTags`
  MODIFY `idUserTag` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;